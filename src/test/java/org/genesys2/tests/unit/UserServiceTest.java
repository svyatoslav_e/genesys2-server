/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
package org.genesys2.tests.unit;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.exception.UserException;
import org.genesys2.server.model.UserRole;
import org.genesys2.server.model.impl.LoginType;
import org.genesys2.server.model.impl.User;
import org.genesys2.server.model.wrapper.UserWrapper;
import org.genesys2.server.security.AuthUserDetails;
import org.genesys2.server.service.PasswordPolicy.PasswordPolicyException;
import org.genesys2.spring.SecurityContextUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

// FIXME Transactional?
//@Ignore
public class UserServiceTest extends AbstractServicesTest {

	private static final Log LOG = LogFactory.getLog(UserServiceTest.class);

	private static final String SYSTEM_PASSWORD = "THIS-IS-NOT-A-PASSWORD";

	private String email;

	private String initialPassword;

	private String fullName;

	private User user;

	@Before
	public void setUp() {
		email = "test@example.com";
		initialPassword = "Alexandr19011990!";
		fullName = "Alechandro";

		user = new User();

		user.setEmail(email);
		user.setPassword(initialPassword);
		user.setName(fullName);
	}

	@After
	public void teardown() {

		try {
			if (userService.exists(email)) {
				User forRemoveUser = userService.getUserByEmail(email);
				userService.removeUser(forRemoveUser);
			}
		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void listAvailableRolesTest() {
		LOG.info("Start test-method listAvailableRolesTest");

		assertTrue(userService.listAvailableRoles().size() == UserRole.values().length);

		LOG.info("Test listAvailableRolesTest passed!");
	}

	@Test
	public void getUserdetailsTest() throws PasswordPolicyException {
		LOG.info("Start test-method getUserdetailsTest");

		try {
			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			AuthUserDetails userDetails = (AuthUserDetails) userService.getUserDetails(user);
			assertTrue(userDetails != null);
			assertTrue(userDetails.getUser() == user);

		} catch (UserException e) {
			e.printStackTrace();
		}
		LOG.info("Test getUserdetailsTest passed!");
	}

	// @Ignore
	@Test
	public void listUsersTest() throws PasswordPolicyException {
		LOG.info("Start test-method listUsersTest");

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			Pageable pageable = (Pageable) new PageRequest(0, 10, new Sort("email"));
			List<User> users = userService.listUsers(pageable).getContent();
			assertTrue(users.size() == 1);

		} catch (UserException e) {
			e.printStackTrace();
		}
		LOG.info("Test listUsersTest passed!");
	}

	@Test
	public void getWrappedByIdTest() throws PasswordPolicyException {
		LOG.info("Start test-method getWrappedByIdTest");

		try {
			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			UserWrapper userWrapper = userService.getWrappedById(user.getId());
			assertTrue(userWrapper != null);

		} catch (UserException e) {
			e.printStackTrace();
		}

		LOG.info("Test getWrappedByIdTest passed!");
	}

	@Test
	public void listWrappedTest() throws PasswordPolicyException {
		LOG.info("Start test-method listWrappedTest");

		int startRow = 0;
		int pageSize = 10;

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			Page<UserWrapper> userWrappers = userService.listWrapped(startRow, pageSize);
			assertTrue(userWrappers != null);
			assertTrue(userWrappers.getContent().get(0).getEmail().equals(email));

		} catch (UserException e) {
			e.printStackTrace();
		}

		LOG.info("Test listWrappedTest passed!");
	}

	@Test
	public void createAccountTest() throws PasswordPolicyException {

		LOG.info("Start test-method createAccountTest");

		User newUser = userService.createAccount(email, initialPassword, fullName, LoginType.PASSWORD);

		assertNotNull("User is ull", newUser);

		assertTrue(newUser.getId() != null);

		assertTrue(newUser.getEmail().equals(email));

		assertTrue(newUser.getName().equals(fullName));

		assertTrue(newUser.getPassword().equals(initialPassword));

		LOG.info("Test createAccountTest Passed!");

	}

	@Test
	public void addUserTest() throws PasswordPolicyException {
		LOG.info("Start test-method addUserTest");

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test addUserTest passed!");
	}

	@Test
	public void updateUserTest() throws PasswordPolicyException {
		LOG.info("Start test-method updateUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);
			userFromDB.setPassword("NewPassword");
			userFromDB.setName("New Name");
			userService.updateUser(userFromDB);

			assertTrue(userService.getUserByEmail(email).getPassword().equals("NewPassword"));
			assertTrue(userService.getUserByEmail(email).getName().equals("New Name"));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateUserTest passed!");
	}

	@Test
	public void updateDataTest() throws PasswordPolicyException {
		LOG.info("Start test-method updateDataTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);

			fullName = "This is new name";
			email = "newMail@new.com";

			userService.updateData(userFromDB.getId(), fullName, email);
			System.out.println(userService.getUserByEmail(email).getEmail());

			userFromDB = userService.getUserByEmail(email);
			assertTrue(userFromDB.getName().equals(fullName));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateDataTest passed!");
	}

	@Test
	public void updatePassword() throws PasswordPolicyException {
		LOG.info("Start test-method updatePassword");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			User userFromDB = userService.getUserByEmail(email);

			assertTrue(userService.getUserByEmail(email).getPassword().equals(initialPassword));
			initialPassword = "This is new password1!";
			userService.updatePassword(userFromDB.getId(), initialPassword);

			userFromDB = userService.getUserByEmail(email);
			assertTrue(userFromDB.getPassword().equals(initialPassword));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updatePassword passed!");
	}

	@Test
	public void setAccountEnabledTest() throws PasswordPolicyException {
		LOG.info("Start test-method setAccountEnabledTest");
		boolean enabled = true;
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.getUserByUuid(user.getUuid()) != null);
			userService.setAccountEnabled(user.getUuid(), enabled);
			assertTrue((userService.getUserByUuid(user.getUuid())).isEnabled());

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setAccountEnabledTest passed!");
	}

	@Test(expected = SecurityException.class)
	public void setDesabledAdminTest() throws PasswordPolicyException {
		LOG.info("Start test-method setDesabledAdminTest");
		boolean enabled = false;
		user.getRoles().add(UserRole.ADMINISTRATOR);

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			userService.setAccountEnabled(user.getUuid(), enabled);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setDesabledAdminTest passed!");
	}

	@Test
	public void setAccountLockLocalTest() throws PasswordPolicyException {
		LOG.info("Start test-method setAccountLockLocalTest");
		boolean locked = true;

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			userService.setAccountLockLocal(user.getUuid(), locked);
			assertTrue(userService.getUserByEmail(email).isAccountLocked());

			locked = false;
			userService.setAccountLockLocal(user.getUuid(), locked);
			assertFalse(userService.getUserByEmail(email).isAccountLocked());

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test setAccountLockLocalTest passed!");
	}

	@Test
	public void removeUserTest() throws PasswordPolicyException {
		LOG.info("Start test-method removeUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			userService.removeUser(user);
			assertFalse(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test removeUserTest passed!");
	}

	@Test
	public void removeUserByIdTest() throws PasswordPolicyException {
		LOG.info("Start test-method removeUserByIdTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			userService.removeUserById(user.getId());
			assertFalse(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test removeUserByIdTest passed!");
	}

	@Test
	public void getMeWithAuthTest() throws PasswordPolicyException {
		LOG.info("Start test-method getMeWithoutAuthTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			AuthUserDetails authUserDetails = SecurityContextUtil.getAuthUser();
			authUserDetails.setUser(user);
			assertTrue(userService.getMe().getUuid().equals(user.getUuid()));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getMeWithoutAuthTest passed!");
	}

	@Test
	public void getUserByEmailTest() throws PasswordPolicyException {
		LOG.info("Start test-method getUserByEmailTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByEmailTest passed!");
	}

	@Test
	public void getUserByUuidTest() throws PasswordPolicyException {
		LOG.info("Start test-method getUserByUuidTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			assertTrue(userService.getUserByUuid(user.getUuid()) != null);
			assertTrue(userService.getUserByUuid(user.getUuid()).getUuid().equals(user.getUuid()));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByUuidTest passed!");
	}

	@Test(expected = UsernameNotFoundException.class)
	public void getUserByUuidExceptionTest() {
		LOG.info("Start test-method getUserByUuidExceptionTest");

		assertFalse(userService.exists(fullName));
		userService.getUserByUuid(user.getUuid());

		LOG.info("Test getUserByUuidExceptionTest passed!");
	}

	@Test
	public void getUserByIdTest() throws PasswordPolicyException {
		LOG.info("Start test-method getUserByIdTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
			assertTrue(userService.getUserById(user.getId()) != null);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test getUserByIdTest passed!");
	}

	@Test
	public void existsTest() throws PasswordPolicyException {
		LOG.info("Start test-method existsTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));
		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test existsTest passed!");
	}

	@Test
	public void userEmailValidatedTest() throws PasswordPolicyException {

		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			List<GrantedAuthority> authorities = new ArrayList<>();
			GrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(UserRole.ADMINISTRATOR.getName());
			authorities.add(simpleGrantedAuthority);

			AuthUserDetails authUserDetails = new AuthUserDetails(user.getName(), user.getPassword(), authorities);

			// set actual DB user
			authUserDetails.setUser(user);

			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(authUserDetails, authUserDetails, authorities);

			SecurityContextHolder.getContext().setAuthentication(authToken);

			assertTrue(authUserDetails.getUser() == user);

			userService.updateUser(user);

			assertFalse(userService.getUserByEmail(email).getRoles().contains(UserRole.VALIDATEDUSER));
			userService.userEmailValidated(user.getUuid());
			assertTrue(userService.getUserByEmail(email).getRoles().contains(UserRole.VALIDATEDUSER));

		} catch (UserException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void updateRolesTest() throws PasswordPolicyException {
		LOG.info("Start test-method updateRolesTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.getUserByEmail(email).getRoles().isEmpty());
			List<String> selectedRoles = new ArrayList<>();
			selectedRoles.add(UserRole.ADMINISTRATOR.getName());
			userService.updateRoles(user, selectedRoles);
			assertTrue(userService.getUserByEmail(email).getRoles().contains(UserRole.ADMINISTRATOR));

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test updateRolesTest passed!");
	}

	@Test
	public void autocompleteUserTest() throws PasswordPolicyException {
		LOG.info("Start test-method autocompleteUserTest");
		try {

			assertFalse(userService.exists(email));
			userService.addUser(user);
			assertTrue(userService.exists(email));

			assertTrue(userService.autocompleteUser("").isEmpty());
			assertTrue(userService.autocompleteUser("m").isEmpty());
			assertTrue(userService.autocompleteUser("ma").isEmpty());
			assertTrue(userService.autocompleteUser("mai").isEmpty());

			assertTrue(userService.autocompleteUser(email).size() == 1);

		} catch (UserException e) {
			fail(e.getMessage());
		}
		LOG.info("Test autocompleteUserTest passed!");
	}

	@Test
	public void createUserWithPasswordTypeTest() throws PasswordPolicyException {
		LOG.info("Start test-method createUserWithPasswordTypeTest");

		LoginType loginType = LoginType.PASSWORD;
		User createdUser = userService.createAccount(email, initialPassword, fullName, loginType);
		assertEquals(createdUser.getLoginType(), loginType);

		LOG.info("Test createUserWithPasswordTypeTest passed!");
	}

	@Test
	public void createUserWithGoogleTypeTest() throws PasswordPolicyException {
		LOG.info("Start test-method createUserWithGoogleTypeTest");

		LoginType loginType = LoginType.GOOGLE;
		User createdUser = userService.createAccount(email, initialPassword, fullName, loginType);
		assertEquals(createdUser.getLoginType(), loginType);
		assertEquals("Password must be equal to 'THIS-IS-NOT-A-PASSWORD'", createdUser.getPassword(), SYSTEM_PASSWORD);

		LOG.info("Test createUserWithGoogleTypeTest passed!");
	}

	@Test
	public void changePasswordForUserWithPasswordTypeTest() throws PasswordPolicyException {
		LOG.info("Start test-method changePasswordForUserWithPasswordTypeTest");

		LoginType loginType = LoginType.PASSWORD;
		User createdUser = userService.createAccount(email, initialPassword, fullName, loginType);

		String newPassword = "New password1!";
		try {
			userService.updatePassword(createdUser.getId(), newPassword);
		} catch (UserException e) {
			fail(e.getMessage());
		}

		try {
			createdUser = userService.getUserById(createdUser.getId());
		} catch (UserException e) {
			fail(e.getMessage());
		}

		assertEquals(createdUser.getPassword(), newPassword);

		LOG.info("Test changePasswordForUserWithPasswordTypeTest passed!");
	}

	@Test
	public void changePasswordForUserWithGoogleTypeTest() throws PasswordPolicyException {
		LOG.info("Start test-method changePasswordForUserWithGoogleTypeTest");

		LoginType loginType = LoginType.GOOGLE;
		User createdUser = userService.createAccount(email, initialPassword, fullName, loginType);

		String newPassword = "New password";
		try {
			userService.updatePassword(createdUser.getId(), newPassword);
		} catch (UserException e) {
			fail(e.getMessage());
		}

		try {
			createdUser = userService.getUserById(createdUser.getId());
		} catch (UserException e) {
			fail(e.getMessage());
		}

		assertEquals(createdUser.getPassword(), SYSTEM_PASSWORD);

		LOG.info("Test changePasswordForUserWithGoogleTypeTest passed!");
	}

}
