/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.test;

import java.io.IOException;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.genesys2.server.aspect.AsAdminAspect;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepository;
import org.genesys2.server.persistence.domain.GenesysLowlevelRepositoryCustomImpl;
import org.genesys2.server.service.AclService;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.GenesysFilterService;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.HtmlSanitizer;
import org.genesys2.server.service.InstituteService;
import org.genesys2.server.service.OrganizationService;
import org.genesys2.server.service.TaxonomyService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.UserService;
import org.genesys2.server.service.audit.SpringSecurityAuditorAware;
import org.genesys2.server.service.impl.AclServiceImpl;
import org.genesys2.server.service.impl.ContentServiceImpl;
import org.genesys2.server.service.impl.CropServiceImpl;
import org.genesys2.server.service.impl.GenesysFilterServiceImpl;
import org.genesys2.server.service.impl.GenesysServiceImpl;
import org.genesys2.server.service.impl.GeoServiceImpl;
import org.genesys2.server.service.impl.InstituteServiceImpl;
import org.genesys2.server.service.impl.OWASPSanitizer;
import org.genesys2.server.service.impl.OrganizationServiceImpl;
import org.genesys2.server.service.impl.TaxonomyServiceImpl;
import org.genesys2.server.service.impl.TraitServiceImpl;
import org.genesys2.server.service.impl.UserServiceImpl;
import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@Configuration
public class GenesysBeansConfig {
	@Bean
	public AsAdminAspect asAdminAspect() {
		return new AsAdminAspect();
	}

	@Bean
	public AuditorAware<?> auditorAware() {
		return new SpringSecurityAuditorAware();
	}

	@Bean
	public GeoService geoService() {
		return new GeoServiceImpl();
	}

	@Bean
	public GenesysService genesysService() {
		return new GenesysServiceImpl();
	}
	
	@Bean
	public GenesysFilterService genesysFilterService() {
		return new GenesysFilterServiceImpl();
	}
	
	@Bean
	public TraitService traitService() {
		return new TraitServiceImpl();
	}
	
	@Bean
	public InstituteService instituteService() {
		return new InstituteServiceImpl();
	}
	
	@Bean
	public GenesysLowlevelRepository genesysLowlevelRepository() {
		return new GenesysLowlevelRepositoryCustomImpl();
	}

	@Bean
	public TaxonomyService taxonomyService() {
		return new TaxonomyServiceImpl();
	}

	@Bean
	public UserService userService() {
		return new UserServiceImpl();
	}

	@Bean
	public AclService aclService() {
		return new AclServiceImpl();
	}

	@Bean
	public CacheManager cacheManager() {
		return new NoOpCacheManager();
	}

	@Bean
	public CropService cropService() {
		return new CropServiceImpl();
	}
	
	@Bean
	public OrganizationService organizationService() {
		return new OrganizationServiceImpl();
	}

	@Bean
	public HtmlSanitizer htmlSanitizer() {
		return new OWASPSanitizer();
	}

	@Bean
	public ContentService contentService() {
		return new ContentServiceImpl();
	}

	@Bean
	public VelocityEngine velocityEngine() throws VelocityException, IOException {
		final VelocityEngineFactoryBean velocityEngineFactoryBean = new VelocityEngineFactoryBean();
		velocityEngineFactoryBean.setVelocityProperties(velocityProperties());
		velocityEngineFactoryBean.afterPropertiesSet();
		VelocityEngine engine = velocityEngineFactoryBean.getObject();
		if (engine == null) {
			throw new RuntimeException("Velocity engine could not be created");
		}
		return engine;
	}

	private Properties velocityProperties() {
		final Properties properties = new Properties();
		properties.setProperty("resource.loader", "class");
		properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		properties.setProperty("velocimacro.permissions.allow.inline.local.scope", "false");
		properties.setProperty("runtime.log.logsystem.log4j.logger", "velocity");
		properties.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.Log4JLogChute");
		return properties;
	}

}
