/**
 * Copyright 2014 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.elasticsearch.common.lang3.StringUtils;
import org.genesys2.server.model.impl.Crop;
import org.genesys2.server.model.impl.CropRule;
import org.genesys2.server.model.impl.CropTaxonomy;
import org.genesys2.server.service.ContentService;
import org.genesys2.server.service.CropService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GenesysService;
import org.genesys2.server.service.TraitService;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilter;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.spring.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/c")
public class CropController extends BaseController {

	@Autowired
	private CropService cropService;

	@Autowired
	private ContentService contentService;

	@Autowired
	private GenesysService genesysService;

	@Autowired
	private TraitService traitService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap model) {
		_logger.debug("Viewing all crops");
		model.addAttribute("crops", cropService.list(getLocale()));
		return "/crop/list";
	}

	@RequestMapping("/{shortName}")
	public String view(ModelMap model, @PathVariable(value = "shortName") String shortName) {
		_logger.debug("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters appliedFilters = cropFilter(crop);

		model.addAttribute("crop", crop);
		model.addAttribute("cropCount", genesysService.countAccessions(appliedFilters));
		model.addAttribute("jsonFilter", appliedFilters.toString());
		model.addAttribute("cropRules", cropService.getCropRules(crop));
		model.addAttribute("cropTaxonomies", cropService.getCropTaxonomies(crop, new PageRequest(0, 21, new Sort("taxonomy.genus", "taxonomy.species"))));
		model.addAttribute("blurp", contentService.getArticle(crop, "blurp", getLocale()));

		// model.addAttribute("statsGenus",
		// elasticService.termStatisticsAuto(appliedFilters,
		// FilterConstants.TAXONOMY_GENUS, 5));

		return "/crop/index";
	}

	private AppliedFilters cropFilter(Crop crop) {
		AppliedFilters appliedFilters = new AppliedFilters();
		appliedFilters.add(new AppliedFilter().setFilterName(FilterConstants.CROPS).addFilterValue(new FilterHandler.LiteralValueFilter(crop.getShortName())));
		return appliedFilters;
	}

	@RequestMapping("/{shortName}/edit")
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#crop, 'ADMINISTRATION')")
	public String edit(ModelMap model, @PathVariable(value = "shortName") String shortName) {
		view(model, shortName);
		try {
			model.addAttribute("cropRulesJson", new ObjectMapper().writeValueAsString(model.get("cropRules")));
		} catch (JsonProcessingException e) {
		}

		return "/crop/edit";
	}

	@RequestMapping("/{shortName}/new")
	@PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasPermission(#crop, 'ADMINISTRATION')")
	public String newCrop(ModelMap model, @PathVariable(value = "shortName") String shortName) throws Exception {
		_logger.debug("Making crop " + shortName);

		if (cropService.getCrop(shortName) != null) {
			throw new Exception("Crop exists.");
		}
		Crop crop = new Crop();
		crop.setShortName(shortName);
		model.addAttribute("crop", crop);
		return "/crop/edit";
	}

	@RequestMapping(value = "/{shortName}/update", method = RequestMethod.POST)
	public String update(ModelMap model, @PathVariable(value = "shortName") String shortName, @RequestParam(value = "cropName", required = false) String cropName,
			@RequestParam("blurp") String aboutBody, @RequestParam(value = "summary", required = false) String summary) {

		Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			_logger.debug("Creating crop " + shortName + " " + cropName);
			crop = cropService.addCrop(shortName, cropName, null, null);
		}
		_logger.debug("Updating crop blurb " + shortName);

		cropService.updateBlurp(crop, aboutBody, summary, getLocale());

		return "redirect:/c/" + shortName;
	}

	@RequestMapping(value = "/{shortName}/update", params = { "otherNames" }, method = RequestMethod.POST)
	public String updateOtherNames(ModelMap model, @PathVariable(value = "shortName") String shortName, @RequestParam("otherNames") String otherNames) {

		_logger.debug("Updating crop aliases for " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		cropService.updateAliases(crop, Arrays.stream(otherNames.split("\\n|,|;")).distinct().map(otherName -> StringUtils.trim(otherName))
				.filter(otherName -> StringUtils.isNotBlank(otherName)).sorted().collect(Collectors.toList()));

		return edit(model, shortName);
	}

	@RequestMapping(value = "/{shortName}/update", params = { "action=update-rules", "cropRules" }, method = RequestMethod.POST)
	public String updateCropRules(ModelMap model, @PathVariable(value = "shortName") String shortName, @RequestParam("cropRules") String cropRulesJson)
			throws JsonParseException, JsonMappingException, IOException {

		_logger.debug("Updating crop rules for " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		List<CropRule> rules = new ObjectMapper().readValue(cropRulesJson, new TypeReference<List<CropRule>>() {
		});
		cropService.setCropRules(crop, rules);

		return edit(model, shortName);
	}

	@RequestMapping(value = "/{shortName}/update", params = { "action=update-taxonomy" }, method = RequestMethod.POST)
	public String updateTaxonomies(ModelMap model, @PathVariable(value = "shortName") String shortName) {

		_logger.debug("Updating crop taxonomies for " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		cropService.rebuildTaxonomies(crop);

		return edit(model, shortName);
	}

	@RequestMapping("/{shortName}/ajax/taxonomies")
	public String ajaxTaxonomies(ModelMap model, @PathVariable(value = "shortName") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null || page < 1) {
			throw new ResourceNotFoundException();
		}
		final Page<CropTaxonomy> res = cropService.getCropTaxonomies(crop, new PageRequest(page - 1, 21, new Sort("taxonomy.genus", "taxonomy.species")));
		model.addAttribute("cropTaxonomies", res);
		if (res.getNumberOfElements() == 0) {
			throw new ResourceNotFoundException();
		}

		return "/crop/ajax.taxonomies";
	}

	@RequestMapping("/{shortName}/data")
	public String viewData(ModelMap model, @PathVariable(value = "shortName") String shortName, @RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.warn("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters cropFilter = cropFilter(crop);
		model.addAttribute("filter", cropFilter.toString());

		return "redirect:/explore";
	}

	@RequestMapping("/{shortName}/overview")
	public String viewOverview(ModelMap model, @PathVariable(value = "shortName") String shortName) {
		_logger.warn("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}

		AppliedFilters cropFilter = cropFilter(crop);
		model.addAttribute("filter", cropFilter.toString());

		return "redirect:/explore/overview";
	}

	@RequestMapping("/{shortName}/descriptors")
	public String viewDescriptors(ModelMap model, @PathVariable(value = "shortName") String shortName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int page) {
		_logger.debug("Viewing crop " + shortName);
		final Crop crop = cropService.getCrop(shortName);
		if (crop == null) {
			throw new ResourceNotFoundException();
		}
		model.addAttribute("crop", crop);
		model.addAttribute("pagedData", traitService.listTraits(crop, new PageRequest(page - 1, 50, new Sort("title"))));
		return "/descr/index";
	}

}
