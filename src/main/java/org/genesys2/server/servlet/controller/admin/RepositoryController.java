/**
 * Copyright 2016 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.servlet.controller.admin;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.genesys2.server.filerepository.InvalidRepositoryFileDataException;
import org.genesys2.server.filerepository.InvalidRepositoryPathException;
import org.genesys2.server.filerepository.NoSuchRepositoryFileException;
import org.genesys2.server.filerepository.model.RepositoryFile;
import org.genesys2.server.filerepository.model.RepositoryFileData;
import org.genesys2.server.filerepository.service.ImageGalleryService;
import org.genesys2.server.filerepository.service.RepositoryService;
import org.genesys2.server.servlet.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriUtils;

@Controller
@RequestMapping(RepositoryController.CONTROLLER_PATH)
@PreAuthorize("hasRole('ADMINISTRATOR')")
public class RepositoryController extends BaseController {
	public static final String CONTROLLER_PATH = "/admin/r";
	public static final String JSP_PATH = "/admin/repository";
	public static final Log LOG = LogFactory.getLog(RepositoryController.class);

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private ImageGalleryService imageGalleryService;

	@RequestMapping(value = "/files/**", method = RequestMethod.GET)
	public String listAllFiles(HttpServletRequest request, ModelMap model) throws UnsupportedEncodingException {
		String fullpath = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		fullpath = fullpath.substring(CONTROLLER_PATH.length() + "/files".length());
		
		// The /** mapping does not decode the URL
		fullpath=UriUtils.decode(fullpath, "UTF-8");
		
		return listAllFiles(fullpath, model);
	}

	@RequestMapping(value = "/files", method = RequestMethod.GET)
	public String listAllFiles(@RequestParam(defaultValue = "/") String repositoryPath, ModelMap model) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Listing files for path=" + repositoryPath);
		}

		List<RepositoryFile> fileList = repositoryService.getFiles(repositoryPath);
		model.addAttribute("fileList", fileList);
		model.addAttribute("currentPath", repositoryPath);
		model.addAttribute("subPaths", repositoryService.listPaths(repositoryPath, new PageRequest(0, 10)));

		model.addAttribute("imageGallery", imageGalleryService.loadImageGallery(repositoryPath));

		return JSP_PATH + "/index";
	}

	@RequestMapping(value = "/edit", method = RequestMethod.GET)
	public String getEditPage(@RequestParam String uuid, ModelMap model) throws NoSuchRepositoryFileException {
		RepositoryFile file = repositoryService.getFile(UUID.fromString(uuid));
		model.addAttribute("file", file);

		return JSP_PATH + "/edit";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String uploadFile(@RequestParam MultipartFile file, @RequestParam String repositoryPath, RedirectAttributes redirectAttributes) throws IOException {
		String mimeType = file.getContentType();

		try {
			if (mimeType.startsWith("image")) {
				repositoryService.addImage(repositoryPath, file.getOriginalFilename(), file.getContentType(), file.getBytes(), null);
			} else {
				repositoryService.addFile(repositoryPath, file.getOriginalFilename(), file.getContentType(), file.getBytes(), null);
			}
		} catch (InvalidRepositoryPathException e) {
			LOG.error("Invalid repository path!", e);
			redirectAttributes.addFlashAttribute("errorMessage", "Invalid repository path!");

		} catch (InvalidRepositoryFileDataException e) {
			LOG.error("Invalid file data!", e);
			redirectAttributes.addFlashAttribute("errorMessage", "Invalid file data!");
		}

		return "redirect:" + CONTROLLER_PATH + "/files" + repositoryPath;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateMetadata(@ModelAttribute RepositoryFileData fileData, @RequestParam String uuid, RedirectAttributes redirectAttributes)
			throws NoSuchRepositoryFileException {
		RepositoryFile updatedFile = repositoryService.getFile(UUID.fromString(uuid));
		
		updatedFile = repositoryService.updateMetadata(updatedFile.getUuid(), fileData);

		return "redirect:" + CONTROLLER_PATH + "/files" + updatedFile.getPath();
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public String deleteFile(@RequestParam String uuid, RedirectAttributes redirectAttributes) throws NoSuchRepositoryFileException, IOException {
		RepositoryFile repositoryFile = repositoryService.getFile(UUID.fromString(uuid));
		repositoryService.removeFile(repositoryFile);

		return "redirect:" + CONTROLLER_PATH + "/files" + repositoryFile.getPath();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}
