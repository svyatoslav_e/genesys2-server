/**
 * Copyright 2015 Global Crop Diversity Trust
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/


package org.genesys2.server.servlet.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.genesys2.server.model.filters.GenesysFilter;
import org.genesys2.server.service.ElasticService;
import org.genesys2.server.service.FilterConstants;
import org.genesys2.server.service.GeoService;
import org.genesys2.server.service.impl.FilterHandler.AppliedFilters;
import org.genesys2.server.service.impl.FilterHandler;
import org.genesys2.server.service.impl.SearchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.facet.result.Term;
import org.springframework.data.elasticsearch.core.facet.result.TermResult;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/explore/charts")
public class ChartsController extends BaseController {

	@Autowired
	private ElasticService elasticService;

	@Autowired
	private GeoService geoService;

	@Autowired
	private FilterHandler filterHandler;
	
	@Autowired
	private ObjectMapper mapper;

	@RequestMapping("/map")
	public String chartsCollection(ModelMap model, @RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter) throws JsonParseException, JsonMappingException, IOException {

		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);
		String[] selectedFilters = appliedFilters.getFilterNames();
		final List<GenesysFilter> currentFilters = filterHandler.selectFilters(selectedFilters);
		model.addAttribute("appliedFilters", appliedFilters);
		model.addAttribute("currentFilters", currentFilters);

		// JSP works with JsonObject
		final Map<?, ?> filters = mapper.readValue(appliedFilters.toString(), Map.class);
		model.addAttribute("filters", filters);
		model.addAttribute("jsonFilter", appliedFilters.toString());

		return "/charts/map";
	}

	@RequestMapping(value = "/data/country-collection-size", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public List<Map<String, Object>> accessionsCollection(@RequestParam(value = "filter", required = false, defaultValue = "{}") String jsonFilter)
			throws JsonParseException, JsonMappingException, IOException, SearchException {

		AppliedFilters appliedFilters = mapper.readValue(jsonFilter, AppliedFilters.class);

		// Load all term results
		TermResult countryStatistics = elasticService.termStatisticsAuto(appliedFilters, FilterConstants.INSTITUTE_COUNTRY_ISO2, Integer.MAX_VALUE);

		List<Map<String, Object>> resultList = new ArrayList<>();

		for (Term term : countryStatistics.getTerms()) {
			Map<String, Object> countryInfo = new HashMap<>();

			// ISO2 country code needs to be upper-case for highcharts library
			countryInfo.put("code", term.getTerm().toUpperCase());
			countryInfo.put("country", geoService.getCountry(term.getTerm()).getName(getLocale()));
			countryInfo.put("z", term.getCount());
			resultList.add(countryInfo);
		}

		return resultList;
	}
}
