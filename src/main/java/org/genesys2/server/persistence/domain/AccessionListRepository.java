/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.persistence.domain;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.genesys2.server.model.genesys.AccessionId;
import org.genesys2.server.model.impl.AccessionList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface AccessionListRepository extends JpaRepository<AccessionList, Long>, AccessionListCustomRepository {

	@Query("from AccessionList al where al.createdBy=?1")
	List<AccessionList> listByOwner(Long userId);

	@Query("select a.id from AccessionList al join al.accessionIds a where al = ?1")
	Set<Long> getAccessionIds(AccessionList list);

	AccessionList findByUuid(UUID uuid);

	@Query("from AccessionList al where al.uuid in (?1)")
	List<AccessionList> findByUuids(List<UUID> uuid);

	@Query(nativeQuery = true, value = "insert into accelistitems (listid, acceid) values (?1, ?2)")
	@Modifying
	void addOne(AccessionList list, AccessionId accession);

	@Query(nativeQuery = true, value = "insert into accelistitems (listid, acceid) values (?1, ?2)")
	@Modifying
	void addOne(AccessionList acceList, long acceId);

	@Query(nativeQuery=true, value="select count(acceid) from accelistitems where listid = ?1")
	int sizeOf(AccessionList list);

	@Query(nativeQuery=true, value="select count(distinct acceid) from accelistitems where listid in (?1)")
	int distinctCount(List<AccessionList> accessionLists);

	@Query("from AccessionList al where al.shared=1 order by al.title asc")
	List<AccessionList> findShared();

	@Query("select uuid from AccessionList al where al.title=?1")
	UUID getUUIDByTitle(String title);

}
