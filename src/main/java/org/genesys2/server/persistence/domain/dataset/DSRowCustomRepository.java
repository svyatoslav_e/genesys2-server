package org.genesys2.server.persistence.domain.dataset;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.genesys2.server.model.dataset.DS;
import org.genesys2.server.model.dataset.DSDescriptor;
import org.genesys2.server.model.dataset.DSRow;
import org.genesys2.server.model.dataset.DSValue;

public interface DSRowCustomRepository {

	DSRow findRow(DS ds, Object[] qualifiers);

	Map<Long, DSValue<?>> rowValueMap(Collection<DSRow> dsrs, DSDescriptor dsd);

	List<Object[]> getQualifiers(DS ds);

	/**
	 * Array of dsv.values as per DSDescriptor order
	 * 
	 * @param rowId
	 * @param columnDescriptors
	 * @return
	 */
	Object[] getRowValues(long rowId, Long[] columnDescriptors);

	/**
	 * List of arrays of dsv.values as per rowId order and DSDescriptor order
	 * 
	 * @param rowId
	 * @param columnDescriptors
	 * @return
	 */
	List<Object[]> getRowValues(List<Long> rowId, Long[] columnDescriptors);

}
