/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.impl;

import org.genesys2.server.model.AclAwareModel;
import org.genesys2.server.model.VersionedAuditedModel;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "project")
public class Project extends VersionedAuditedModel implements AclAwareModel {

	private static final long serialVersionUID = -2262512202111475334L;

	@Column(length = 200, nullable = false)
	private String name;

	@Column(length = 200, nullable = true)
	private String url;

	@Column(length = 50, nullable = false, unique = true)
	private String code;

	@Column(name = "uuid", nullable = false)
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "project_accelist", joinColumns = @JoinColumn(name = "projectId", referencedColumnName = "id"))
	private List<UUID> accessionLists;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<UUID> getAccessionLists() {
		return accessionLists;
	}

	public void setAccessionLists(List<UUID> accessionLists) {
		this.accessionLists = accessionLists;
	}
}
