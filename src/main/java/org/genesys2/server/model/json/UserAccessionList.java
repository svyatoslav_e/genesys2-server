/**
 * Copyright 2015 Global Crop Diversity Trust
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

package org.genesys2.server.model.json;

import java.io.Serializable;
import java.util.UUID;

import org.genesys2.server.model.impl.AccessionList;

public class UserAccessionList implements Serializable {
	private static final long serialVersionUID = -2092185410372557368L;
	private Long id;
	public UUID uuid;
	public String title;
	public String description;
	public boolean shared;

	public static UserAccessionList from(AccessionList ual) {
		UserAccessionList ul = new UserAccessionList();
		ul.id = ual.getId();
		ul.uuid = ual.getUuid();
		ul.title = ual.getTitle();
		ul.description = ual.getDescription();
		ul.shared=ual.isShared();
		return ul;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public UUID getUuid() {
		return uuid;
	}
	
	public boolean getShared() {
		return shared;
	}
}
