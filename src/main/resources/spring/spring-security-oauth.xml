<?xml version="1.0" encoding="UTF-8" ?>
<!--
  Copyright 2014 Global Crop Diversity Trust
  
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
    http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
-->

<beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:oauth="http://www.springframework.org/schema/security/oauth2" xmlns:sec="http://www.springframework.org/schema/security"
	xsi:schemaLocation="http://www.springframework.org/schema/security/oauth2 http://www.springframework.org/schema/security/spring-security-oauth2-1.0.xsd
                           http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.1.xsd
                           http://www.springframework.org/schema/security http://www.springframework.org/schema/security/spring-security.xsd">

	<!-- <bean id="accessDecisionManager" class="org.springframework.security.access.vote.UnanimousBased" xmlns="http://www.springframework.org/schema/beans">
		<constructor-arg>
			<list>
				<bean class="org.springframework.security.oauth2.provider.vote.ScopeVoter" />
				<bean class="org.springframework.security.access.vote.RoleVoter" />
				<bean class="org.springframework.security.access.vote.AuthenticatedVoter" />
			</list>
		</constructor-arg>
	</bean> -->

	<bean id="oauthAccessDeniedHandler" class="org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler" />

	<bean id="clientDetailsUserService" class="org.springframework.security.oauth2.provider.client.ClientDetailsUserDetailsService">
		<constructor-arg ref="clientDetails" />
	</bean>

	<bean id="tokenServices" class="org.springframework.security.oauth2.provider.token.DefaultTokenServices">
		<property name="tokenStore" ref="tokenStore" />
		<property name="supportRefreshToken" value="true" />
		<property name="clientDetailsService" ref="clientDetails" />
	</bean>

	<sec:authentication-manager id="clientAuthenticationManager">
		<sec:authentication-provider user-service-ref="clientDetailsUserService">
			<!-- <sec:password-encoder ref="passwordEncoder" /> -->
		</sec:authentication-provider>
	</sec:authentication-manager>

	<oauth:authorization-server client-details-service-ref="clientDetails" token-services-ref="tokenServices">
		<oauth:authorization-code authorization-code-services-ref="verificationCodeService" />
		<oauth:refresh-token />
		<oauth:password disabled="false" authentication-manager-ref="authenticationManager" />
	</oauth:authorization-server>

	<oauth:resource-server id="resourceServerFilter" resource-id="crophub_oauth_server" token-services-ref="tokenServices" />

	<oauth:expression-handler id="oauthExpressionHandler" />
	<oauth:web-expression-handler id="oauthWebExpressionHandler" />

	<sec:http pattern="/oauth/token" authentication-manager-ref="clientAuthenticationManager" create-session="stateless" xmlns="http://www.springframework.org/schema/security">
		<sec:intercept-url pattern="/oauth/token" access="isAuthenticated()" />
		<sec:anonymous enabled="false" />
		<sec:http-basic entry-point-ref="clientAuthenticationEntryPoint" />
		<!-- include this only if you need to authenticate clients via request parameters -->
		<sec:custom-filter ref="clientCredentialsTokenEndpointFilter" after="BASIC_AUTH_FILTER" />
		<sec:access-denied-handler ref="oauthAccessDeniedHandler" />
		<sec:csrf disabled="true" />
	</sec:http>

	<!-- The OAuth2 protected resources are separated out into their own block so we can deal with authorization and error handling
		separately. This isn't mandatory, but it makes it easier to control the behaviour. -->
	<sec:http pattern="/oauth/(users|clients)/.*" authentication-manager-ref="clientAuthenticationManager" request-matcher="regex" create-session="stateless" entry-point-ref="oauthAuthenticationEntryPoint" use-expressions="true" xmlns="http://www.springframework.org/schema/security">
		<sec:anonymous enabled="false" />
		<sec:intercept-url pattern="/oauth/users/([^/].*?)/tokens/.*" access="#oauth2.clientHasRole('ROLE_CLIENT') and (hasRole('ROLE_USER') or #oauth2.isClient()) and #oauth2.hasScope('write')" method="DELETE" />
		<sec:intercept-url pattern="/oauth/users/.*" access="#oauth2.clientHasRole('ROLE_CLIENT') and (hasRole('ROLE_USER') or #oauth2.isClient()) and #oauth2.hasScope('read')" method="GET" />
		<sec:intercept-url pattern="/oauth/clients/.*" access="#oauth2.clientHasRole('ROLE_CLIENT') and #oauth2.isClient() and #oauth2.hasScope('read')" method="GET" />
		<sec:intercept-url pattern="/**" access="denyAll()" />
		<sec:custom-filter ref="resourceServerFilter" before="PRE_AUTH_FILTER" />
		<sec:access-denied-handler ref="oauthAccessDeniedHandler" />
		<sec:expression-handler ref="oauthWebExpressionHandler" />
	</sec:http>

	<sec:http pattern="/api/v0/.*" authentication-manager-ref="clientAuthenticationManager" request-matcher="regex" create-session="stateless" entry-point-ref="oauthAuthenticationEntryPoint" use-expressions="true"
		xmlns="http://www.springframework.org/schema/security">
		<sec:anonymous enabled="false" />
		<sec:intercept-url pattern="/api/v0/.*" access="isAuthenticated()" />
		<sec:intercept-url pattern="/**" access="denyAll()" />
		<sec:custom-filter ref="resourceServerFilter" before="PRE_AUTH_FILTER" />
		<sec:http-basic entry-point-ref="oauthAuthenticationEntryPoint" />
		<sec:access-denied-handler ref="oauthAccessDeniedHandler" />
		<sec:expression-handler ref="oauthWebExpressionHandler" />
		<sec:csrf disabled="true" />
	</sec:http>

	<bean id="oauthAuthenticationEntryPoint" class="org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint">
		<property name="realmName" value="genesys2" />
	</bean>

	<bean id="clientAuthenticationEntryPoint" class="org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint">
		<property name="realmName" value="genesys2/client" />
		<property name="typeName" value="Basic" />
	</bean>

	<bean id="clientCredentialsTokenEndpointFilter" class="org.springframework.security.oauth2.provider.client.ClientCredentialsTokenEndpointFilter">
		<property name="authenticationManager" ref="clientAuthenticationManager" />
	</bean>

</beans>
