#-------------------------------------------------------------------------------
# Copyright 2014 Global Crop Diversity Trust
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#-------------------------------------------------------------------------------

# Errors
http-error.401=Não autorizado
http-error.401.text=Autenticação obrigatória
http-error.403=Acesso negado
http-error.403.text=Você não tem permissão para acessar o recurso
http-error.404=Não encontrado
http-error.404.text=O recurso solicitado não foi encontrado, mas pode vir a ser disponibilizado no futuro.
http-error.500=Erro interno do servidor
http-error.500.text=Encontramos um erro e não há outras mensagens relevantes.
http-error.503=Serviço não disponível
http-error.503.text=O servidor encontra-se indisponível (por sobrecarga ou manutenção.)


# Login
login.username=Nome de usuário
login.password=Senha
login.invalid-credentials=Credenciais inválidas.
login.remember-me=Lembrar
login.login-button=Entrar
login.register-now=Criar uma conta
logout=Sair
login.forgot-password=Esqueci minha senha
login.with-google-plus=Entrar com Google+

# Registration
registration.page.title=Criar uma conta de usuário
registration.title=Criar sua conta
registration.invalid-credentials=Credenciais inválidas.
registration.user-exists=Nome de usuário já existente.
registration.email=Email
registration.password=Senha
registration.current-password=Senha atual
registration.confirm-password=Repetir senha
registration.full-name=Nome completo
registration.create-account=Criar conta
captcha.text=Texto de captcha
errors.badCaptcha=Falha na verificação captcha.
errors.no-such-user=O Genesys não tem uma conta de usuário com o email fornecido.
errors.second-password-doesnt-match=Segunda senha não corresponde
errors.reset-password.invalid-login-type=Senha para usuários com tipo de login GOOGLE não podem ser redefinidos\!

sample.error.not.empty=O campo não pode ficar vazio
sample.error.wrong.email=Formato de email inválido

id=ID

name=Nome
description=Descrição
actions=Ações
add=Adicionar
edit=Editar
save=Salvar
create=Criar
cancel=Cancelar
delete=Excluir
prompt.confirm-delete=O registro selecionado deve mesmo ser excluído?
prompt.loading-data=Carregando dados do Genesys...
prompt.loading-data-failed=Falha no carregamento de dados. Tente outra vez.

jump-to-top=Voltar ao início\!

pagination.next-page=Próximo >
pagination.previous-page=< Anterior


# Language
locale.language.change=Alterar local
i18n.content-not-translated=Este conteúdo não está disponível no seu idioma. Entre em contato caso possa ajudar na tradução.

data.error.404=Os dados solicitados não foram encontrados no sistema.
page.rendertime=O processamento desta página demorou {0}min.

footer.copyright-statement=&copy; 2013 - 2016 Provedores de dados e Crop Trust

menu.home=Início
menu.browse=Navegar
menu.datasets=Dados C&E
menu.descriptors=Descritores
menu.countries=Países
menu.institutes=Institutos
menu.my-list=Minha lista
menu.about=Sobre o Genesys
menu.contact=Entre em contato contato conosco
menu.disclaimer=Aviso de isenção
menu.feedback=Comentários
menu.help=Ajuda
menu.terms=Termos e Condições de Uso
menu.copying=Política de direitos autorais
menu.privacy=Política de privacidade
menu.newsletter=Informativo Genesys
menu.join-the-community=Venha para a comunidade Genesys
menu.faq=Perguntas frequentes
menu.news=Notícias
page.news.title=Notícias
page.news.intro=As últimas da comunidade Genesys - de detalhes dos novos membros a atualizações de acessões conservadas em diferentes lugares do mundo.

# Extra content
menu.what-is-genesys=O que é Genesys?
menu.who-uses-genesys=Quem uso o Genesys?
menu.how-to-use-genesys=Como se usa o Genesys?
menu.history-of-genesys=História do Genesys
menu.about-genesys-data=Sobre os dados Genesys



page.home.title=PGR do Genesys

user.pulldown.administration=Administração
user.pulldown.users=Lista de usuários
user.pulldown.logout=Sair
user.pulldown.profile=Meu perfil
user.pulldown.oauth-clients=Clientes OAuth
user.pulldown.teams=Equipes
user.pulldown.manage-content=Administrar conteúdo

user.pulldown.heading={0}
user.create-new-account=Criar uma conta
user.full-name=Nome completo
user.email=Endereço de email
user.account-status=Status da conta
user.account-disabled=Conta desabilitada
user.account-locked-until=Conta bloqueada até
user.roles=Funções dos usuários
user.login-type=Tipo de login
userprofile.page.title=Perfil do usuário
userprofile.page.intro=Como comunidade, o Genesys conta com seus usuários para dar certo. Seja como pesquisador individual ou representante de uma instituição maior, você pode atualizar seu perfil aqui para exibir seus interesses e os dados de sua coleta.
userprofile.update.title=Atualize seu perfil

user.page.list.title=Contas de usuário registradas

crop.croplist=Lista de lavouras
crop.all-crops=Todas as lavouras
crop.taxonomy-rules=Regras taxonômicas
crop.view-descriptors=Ver descritores de culturas...
crop.page.edit.title=Editar {0}
crop.summary=Resumo (metadata de HTML)
crop.name-en=Nome da cultura (em inglês)
crop.cropname-aliases=Outros nomes aceitáveis

activity.recent-activity=Atividade recente

country.page.profile.title=Perfil do país\: {0}
country.page.list.title=Lista de países
country.page.list.intro=Clicando no país, você pode encontrar institutos registrados naquele país e estatísticas de recursos genéticos vegetais do país como um todo.
country.page.not-current=Esta é uma entrada do histórico.
country.page.faoInstitutes={0} institutos registrados no WIEWS
country.stat.countByLocation={0} acessões em institutos neste país
country.stat.countByOrigin={0} acessões registadas no Genesys vêm deste país.
country.stat.accessionCount=Acessões no Genesys
country.statistics=Estatísticas do país
country.accessions.from=Acessões coletadas em {0}
country.more-information=Mais informações\:
country.replaced-by=Código de país é substituído por\: {0}
country.is-itpgrfa-contractingParty={0} é parte no Tratado Internacional sobre os Recursos Fitogenéticos para Alimentação e Agricultura (ITPGRFA).
select-country=Selecionar país

project.page.list.title=Projetos
project.page.profile.title={0}
project.code=Código do projeto
project.name=Nome do projeto
project.url=Website do projeto
project.summary=Resumo (metadata HTML)
project.accessionLists=Listas de acessões


faoInstitutes.page.list.title=Institutos WIEWS
faoInstitutes.page.profile.title=WIEWS {0}
faoInstitutes.stat.accessionCount=Acessões no Genesys\:
faoInstitutes.stat.datasetCount=Conjuntos de dados adicionais no Genesys\:
faoInstitute.stat-by-crop=Lavouras mais representadas
faoInstitute.stat-by-cropName=Nomes de culturas mais usados
faoInstitute.stat-by-genus=Gêneros mais representados
faoInstitute.stat-by-species=Espécies mais representadas
faoInstitute.accessionCount={0} acessões
faoInstitute.statistics=Estatísticas do instituto
faoInstitutes.page.data.title=Acessões em {0}
faoInstitute.accessions.at=Acessões em {0}
faoInstitutes.viewAll=Ver todos os institutos registrados
faoInstitutes.viewActiveOnly=Ver institutos com acessões no Genesys
faoInstitute.no-accessions-registered=Entre em contato conosco se puder facilitar o provimento de dados para este instituto.
faoInstitute.institute-not-current=Este registro está arquivado.
faoInstitute.view-current-institute=Navegar até {0}.
faoInstitute.country=País
faoInstitute.code=Código WIEWS
faoInstitute.email=Email de contato
faoInstitute.acronym=Acrônimo
faoInstitute.url=Link da web
faoInstitute.summary=Resumo (metadata HTML) 
faoInstitute.member-of-organizations-and-networks=Organizações e Redes\:
faoInstitute.uniqueAcceNumbs.true=Cada número de acessão é único neste instituto
faoInstitute.uniqueAcceNumbs.false=O mesmo número de acessão pode ser usado em diferentes coletas neste instituto.
faoInstitute.requests.mailto=Endereço de email para requisição de materiais\:
faoInstitute.allow.requests=Permitir requisição de material
faoInstitute.sgsv=Código SGSV
faoInstitute.data-title=Banco de dados de informações sobre acessões mentidas em {0}
faoInstitute.overview-title=Visão geral de informações sobre acessões mantidas em {0}
faoInstitute.datasets-title=Dados de caracterização e avaliação fornecidos por {0}
faoInstitute.meta-description=Visão geral de recursos genéticos vegetais mantidos em coleções no banco de dados {0} ({1}) 
faoInstitute.link-species-data=Exibir informações sobre {2} acessões em {0} ({1})
faoInstitute.wiewsLink={0} detalhes no site FAO WIEWS

view.accessions=Navegue pelas acessões
view.datasets=Exibir conjunto de dados
paged.pageOfPages=Página {0} de {1}
paged.ofPages=de {0} páginas
paged.resultsPerPage=Mostrar resultados/página\:
paged.totalElements={0} entradas

accessions.number={0} acessões
accession.metadatas=Dados C&E
accession.methods=Caracterização & Dados de avaliação
unit-of-measure=Unidade de medida

ce.trait=Característica
ce.sameAs=O mesmo que
ce.methods=Métodos
ce.method=Método
method.fieldName=Campo DB

accession.uuid=UUID
accession.accessionName=Número da acessão
accession.acceNumb=Número da acessão
accession.origin=País de origem
accession.orgCty=País de origem
accession.holdingInstitute=Instituto depositário
accession.institute.code=Instituto depositário
accession.holdingCountry=Localização
accession.institute.country.iso3=Localização
accession.taxonomy=Nome científico
accession.taxonomy.sciName=Nome científico
accession.genus=Género
accession.taxonomy.genus=Género
accession.species=Espécie
accession.taxonomy.species=Espécie
accession.subtaxa=Subtaxa
accession.taxonomy.subtaxa=Subtaxa
accession.crop=Nome da lavoura
accession.cropName=Nome de cultura fornecido
accession.crops=Nome da lavoura
accession.otherNames=Também conhecido como
accession.inTrust=Em Confiança
accession.mlsStatus=Status MLS
accession.duplSite=Instituto de duplicação segura
accession.inSvalbard=Duplicado com segurança no Svalbard
accession.inTrust.true=Esta acessão obedece ao Artigo 15 do Tratado Internacional de Recursos Genéticos Vegetais para Alimentação e Agricultura.
accession.mlsStatus.true=Esta acessão pertence ao Sistema Multilateral do ITPGRFA.
accession.inSvalbard.true=Duplicado com segurança no Svalbard Global Seed Vault.
accession.not-available-for-distribution=A acessão NÃO está disponível para distribuição.
accession.this-is-a-historic-entry=Este é o registro histórico de uma acessão
accession.historic=Entrada no histórico
accession.available-for-distribution=A acessão está disponível para distribuição.
accession.elevation=Elevação
accession.geolocation=Geolocalização (lat, long)

accession.storage=Tipo de estoque de Germplasm
accession.storage.=
accession.storage.10=Coleta de sementes
accession.storage.11=Coleta de sementes de curto prazo
accession.storage.12=Coleta de sementes de médio prazo
accession.storage.13=Coleta de sementes de longo prazo
accession.storage.20=Coleta de campo
accession.storage.30=Coleta in vitro
accession.storage.40=Coleta criopreservada
accession.storage.50=Coleta de DNA
accession.storage.99=Outros

accession.breeding=Informação do melhorista
accession.breederCode=Código do melhorista
accession.pedigree=Linhagem
accession.collecting=Informações da coleta
accession.collecting.site=Localização do ponto de coleta
accession.collecting.institute=Instituto de coleta
accession.collecting.number=Número da coleta
accession.collecting.date=Data de coleta da amostra
accession.collecting.mission=ID da missão de coleta
accession.coll.collMissId=ID da missão de coleta
accession.collecting.source=Fonte da coleta/aquisição

accession.collectingSource.=
accession.collectingSource.10=Habitat natural
accession.collectingSource.11=Floresta ou cerrado
accession.collectingSource.12=Capoeira
accession.collectingSource.13=Campo
accession.collectingSource.14=Deserto ou tundra
accession.collectingSource.15=Habitat aquático
accession.collectingSource.20=Campo ou habitat cultivado
accession.collectingSource.21=Campo
accession.collectingSource.22=Pomar
accession.collectingSource.23=Quintal, cozinha ou jardim (urbano, periferia ou rural)
accession.collectingSource.24=Pousio
accession.collectingSource.25=Pastagem
accession.collectingSource.26=Silo
accession.collectingSource.27=Área de debulhagem
accession.collectingSource.28=Parque
accession.collectingSource.30=Mercado ou loja
accession.collectingSource.40=Instituto, estação experimental, organização de pesquisa, banco de genes
accession.collectingSource.50=Empresa de sementes
accession.collectingSource.60=Habitat de ervas daninhas, danificado ou ruderal
accession.collectingSource.61=Beira-de-estrada
accession.collectingSource.62=Margem de campo
accession.collectingSource.99=Outros

accession.donor.institute=Instituto do doador
accession.donor.accessionNumber=ID da acessão do doador
accession.geo=Informação geográfica
accession.geo.datum=Datum de coordenadas
accession.geo.method=Método georeferencial
accession.geo.uncertainty=Incerteza das coordenadas
accession.geo.latitudeAndLongitude=Geolocalização

accession.sampStat=Status biológico
accession.sampleStatus=Status biológico da acessão
accession.sampleStatus.=
accession.sampleStatus.100=Selvagem
accession.sampleStatus.110=Natural
accession.sampleStatus.120=Semi-natural/selvagem
accession.sampleStatus.130=Semi-natural/semeada
accession.sampleStatus.200=Daninha
accession.sampleStatus.300=Variedade primitiva/cultivar tradicional
accession.sampleStatus.400=Material para reprodução/pesquisa
accession.sampleStatus.410=Linhagem do reprodutor
accession.sampleStatus.411=População sintética
accession.sampleStatus.412=Híbrido
accession.sampleStatus.413=Estoque inicial/população base
accession.sampleStatus.414=Linhagem consanguínea
accession.sampleStatus.415=População de segregação
accession.sampleStatus.416=Seleção clonal
accession.sampleStatus.420=Estoque genético
accession.sampleStatus.421=Mutante
accession.sampleStatus.422=Estoques citogenéticos
accession.sampleStatus.423=Outros estoques genéticos
accession.sampleStatus.500=Cultivar avançado/melhorado
accession.sampleStatus.600=OGM
accession.sampleStatus.999=Outros

accession.availability=Disponibilidade para distribuição
accession.aliasType.ACCENAME=Nome de acessão
accession.aliasType.DONORNUMB=Identificador da acessão do doador
accession.aliasType.BREDNUMB=Nome designado pelo melhorista
accession.aliasType.COLLNUMB=Número de coleta
accession.aliasType.OTHERNUMB=Outros nomes
accession.aliasType.DATABASEID=(ID da base de dados)
accession.aliasType.LOCALNAME=(Nome local)

accession.availability.=Desconhecido
accession.availability.true=Disponível
accession.availability.false=Não disponível

accession.historic.true=Histórico
accession.historic.false=Ativar
accession.acceUrl=URL adicional da acessão
accession.scientificName=Nome científico

accession.page.profile.title=Perfil de acessão\: {0}
accession.page.resolve.title=Múltiplas acessões encontradas
accession.resolve=Múltiplas acessões com o nome "{0}" encontradas no Genesys. Selecione uma da lista.
accession.page.data.title=Pesquisador de acessões
accession.page.data.intro=Explore os dados das acessões pelo Genesys. Para refinar sua pesquisa, adicione filtros como, país de origem, lavoura. espécie ou latitude e longitude do local de coleta.
accession.taxonomy-at-institute=Ver {0} a {1}

accession.svalbard-data=Dados de duplicação do Banco Global de Sementes de Svalbard
accession.svalbard-data.taxonomy=Nome científico informado
accession.svalbard-data.regenerationInformation=Regeneração de sementes (mês e ano)
accession.svalbard-data.depositDate=Data de depósito
accession.svalbard-data.boxNumber=Número da caixa
accession.svalbard-data.quantity=Quantidade
accession.remarks=Comentários
accession.pdci.score=Pontuação PDCI

accession.imageGallery=Imagens da acessão

taxonomy.genus=Gênero
taxonomy.species=Espécie
taxonomy.taxonName=Nome científico
taxonomy-list=Lista de taxonomias

selection.checkbox=Clique em adicionar ou remover a acessão da lista
selection.page.title=Acessões selecionadas
selection.page.intro=Conforme explora as milhões de acessões armazenadas no Genesys, você pode criar sua própria lista e rastrear os resultados da sua pesquisa. Suas seleções ficam armazenadas aqui para voltar a elas a qualquer momento.
selection.add=Adicionar {0} à lista
selection.remove=Remover {0} da lista
selection.clear=Limpar a lista
selection.reload-list=Recarregar a lista
selection.map=Exibir mapa de acessões
selection.send-request=Enviar solicitação de germoplasma
selection.empty-list-warning=Você não adicionou nenhuma acessão à lista.
selection.add-many=Adicionar várias acessões
selection.add-many.accessionIds=Listar os números das acessões (ACCENUMB) conforme registrado no Genesys, separados por vírgula, ponto-e-vírgula ou uma nova linha.
selection.add-many.instCode=Código WIES do instituto depositário
selection.add-many.button=Adicionar à lista de seleção

savedmaps=Lembrar mapa atual
savedmaps.list=Lista de mapa
savedmaps.save=Lembrar mapa
taxonomy.subtaxa=Subtaxa

filter.enter.title=Digite o título do filtro
filters.page.title=Filtros de dados
filters.view=Filtros atuais
filter.filters-applied=Você usou filtros.
filter.filters-not-applied=Você pode filtrar os dados.
filters.data-is-filtered=Os dados estão filtrados.
filters.toggle-filters=Filtros
filter.taxonomy=Nome científico
filter.art15=Acessão ITPGRFA Art. 15
filter.acceNumb=Número da acessão
filter.seqNo=Número sequencial detectado
filter.alias=Nome da acessão
filter.crops=Nome da lavoura
filter.cropName=Nome de cultura fornecido
filter.orgCty.iso3=País de origem
filter.institute.code=Nome do instituto depositário
filter.institute.country.iso3=País do instituto depositário
filter.sampStat=Status biológico da acessão
filter.institute.code=Nome do instituto depositário
filter.geo.latitude=Latitude
filter.geo.longitude=Longitude
filter.geo.elevation=Elevação
filter.taxonomy.genus=Genus
filter.taxonomy.species=Espécie
filter.taxonomy.subtaxa=Subtaxa
filter.taxSpecies=Espécie
filter.taxonomy.sciName=Nome científico
filter.sgsv=Duplicado com segurança no Svalbard
filter.mlsStatus=Status MLS da acessão
filter.available=Disponível para distribuição
filter.historic=Registro de histórico
filter.donorCode=Instituto do doador
filter.duplSite=Local da duplicação segura
filter.download-dwca=Fazer o download de ZIP
filter.download-mcpd=Fazer download de MCPD
filter.add=Refinar a pesquisa por categoria
filter.additional=Refinar a pesquisa por característica
filter.apply=Aplicar
filter.close=Fechar
filter.remove=Remover filtro
filter.autocomplete-placeholder=Digite 3 caracteres ou mais...
filter.coll.collMissId=ID da missão de coleta
filter.storage=Tipo de armazenagem de Germplasm
filter.string.equals=Equivale a
filter.string.like=Começa com
filter.inverse=Excluindo
filter.set-inverse=Excluir valores selecionados
filter.internal.message.like=Curtir {0}
filter.internal.message.between=Entre {0}
filter.internal.message.and={0} e {0}
filter.internal.message.more=Mais de {0}
filter.internal.message.less=Menos de {0}
filter.lists=Listado na lista de acessões
filter.more-filters=Mais filtros
filter.pdci=Pontuação PDCI

columns.add=Mudar as colunas exibidas
columns.apply=Aplicar
columns.availableColumns=Selecione colunas para exibir e clique em Aplicar
columns.latitudeAndLongitude=Latitude e longitude
columns.collectingMissionID=ID da missão de coleta

columns.acceNumb=Número da acessão
columns.scientificName=Nome científico
columns.orgCty=País de origem
columns.sampStat=Status biológico da acessão
columns.instCode=Instituto depositário

search.page.title=Pesquisa de texto completo
search.no-results=Não foram encontrados resultados para a sua pesquisa.
search.input.placeholder=Pesquisar no Genesys...
search.search-query-missing=Digite sua pesquisa.
search.search-query-failed=Erro na pesquisa {0}
search.button.label=Pesquisar
search.add-genesys-opensearch=Registre o Genesys Search com seu navegador
search.section.accession=Acessões
search.section.article=Conteúdo
search.section.activitypost=Novos itens
search.section.institute=Bancos de gens
search.section.country=Países

admin.page.title=Administração do Genesys 2
metadata.page.title=Conjuntos de dados
metadata.page.intro=Navegue pelos conjuntos de dados de caracterização e avaliação carregados no Genesys por pesquisadores e organizações de pesquisa. Você pode fazer o download de dados de cada uma em formato Excel ou CVS.
metadata.page.view.title=Detalhes do conjunto de dados
metadata.download-dwca=Download ZIP
page.login=Entrar

traits.page.title=Descritores
trait-list=Descritores


organization.page.list.title=Organizações e Redes
organization.page.profile.title={0}
organization.slug=Acrônimo da organização ou rede
organization.title=Nome completo
organization.summary=Resumo (HTML metadata)
filter.institute.networks=Rede


menu.report-an-issue=Relatar um problema
menu.scm=Código fonte
menu.translate=Traduzir o Genesys

article.edit-article=Editar artigo
article.slug=Slug do artigo (URL)
article.title=Título do artigo
article.body=Corpo do artigo
article.summary=Resumo (metadata HTML) 
article.post-to-transifex=Enviar para Transifex
article.remove-from-transifex=Excluir de Transifex
article.fetch-from-transifex=Ver traduções
article.transifex-resource-updated=O recurso foi atualizado com sucesso no Transifex.
article.transifex-resource-removed=O recurso foi excluído com sucesso do Transifex.
article.transifex-failed=Ocorreu um erro no compartilhamento de dados com Transifex
article.translations-updated=Recursos atualizados com sucesso com dados traduzidos\!
article.share=Compartilhe este artigo

user.accession.list.saved-updated=Sua lista de acessões foi salva com êxito.
user.accession.list.deleted=Sua lista de acessões foi excluída do servidor.
user.accession.list.create-update=Salvar
user.accession.list.delete=Excluir
user.accession.list.title=Lista de acessões

activitypost=Post de atividade
activitypost.add-new-post=Adicionar novo post
activitypost.post-title=Título do post
activitypost.post-body=Corpo

blurp.admin-no-blurp-here=Não há blurp aqui.
blurp.blurp-title=Título do blurp
blurp.blurp-body=Conteúdo do blurp
blurp.update-blurp=Salvar blurp


oauth2.confirm-request=Confirmar acesso
oauth2.confirm-client=Você, <b>{0}</b>, por este meio autoriza <b>{1}</b> a aceder aos seus recursos protegidos.
oauth2.button-approve=Sim, permitir acesso
oauth2.button-deny=Não, negar acesso

oauth2.authorization-code=Código de autorização
oauth2.authorization-code-instructions=Copie este código de autorização\:

oauth2.access-denied=Acesso negado
oauth2.access-denied-text=Você negou acesso aos seus recursos.

oauth-client.page.list.title=Clientes OAuth2
oauth-client.page.profile.title=Cliente OAuth2\: {0}
oauth-client.active-tokens=Lista de tokens emitidos
client.details.title=Título do cliente
client.details.description=Descrição

maps.loading-map=Carregando mapa...
maps.view-map=Ver mapa
maps.accession-map=Mapa de acessão
maps.accession-map.intro=O mapa mostra o local de coleta de acessões geo-referenciadas.
maps.baselayer.list=Mudar a camada básica do mapa

audit.createdBy=Criado por {0}
audit.lastModifiedBy=Última atualização feita por {0}

itpgrfa.page.list.title=Partes no ITPGRFA

request.page.title=Solicitando material aos institutos depositários
request.total-vs-available=De {0} acessões listadas, sabe-se que {1} estão disponíveis para distribuição.
request.start-request=Solicitar germoplasma disponível
request.your-email=Seu endereço de email\:
request.accept-smta=Confirmação de SMTA/MTA
request.smta-will-accept=Aceito os termos e condições SMTA/MTA
request.smta-will-not-accept=Não aceito os termos e condições SMTA/MTA
request.smta-not-accepted=Você não indicou que aceita os termos e condições do SMTA/MTA. Sua requisição de material não será processada.
request.purpose=Especifique o uso de material\:
request.purpose.0=Outros (utilize o campo Notas)
request.purpose.1=Pesquisa de alimentos e agricultura
request.notes=Forneça comentários adicionais e notas\:
request.validate-request=Validar sua solitação
request.confirm-request=Confirmar recebimento do pedido
request.validation-key=Chave de validação\:
request.button-validate=Validar
validate.no-such-key=Chave de validação inválida.

team.user-teams=Equipes do usuário
team.create-new-team=Criar nova equipe
team.team-name=Nome da equipe
team.leave-team=Sair da equipe
team.team-members=Membros da equipe
team.page.profile.title=Equipe\: {0}
team.page.list.title=Todas as equipes
validate.email.key=Digite a chave
validate.email=Validação de email
validate.email.invalid.key=Chave inválida

edit-acl=Editar permissões
acl.page.permission-manager=Gerente de permissões
acl.sid=Identidade de segurança
acl.owner=Proprietário do objeto
acl.permission.1=Ler
acl.permission.2=Escrever
acl.permission.4=Criar
acl.permission.8=Excluir
acl.permission.16=Gerenciar


ga.tracker-code=Código de rastreamento GA

boolean.true=Sim
boolean.false=Não
boolean.null=Desconhecido
userprofile.password=Redefinir a senha
userprofile.enter.email=Digite seu email
userprofile.enter.password=Digite uma nova senha
userprofile.email.send=Enviar email

verification.invalid-key=Chave token inválida.
verification.token-key=Chave de validação
login.invalid-token=Token de acesso inválido

descriptor.category=Categoria do descritor
method.coding-table=Tabela de códigos

oauth-client.info=Informações do cliente
oauth-client.list=Lista de clientes oauth
client.details.client.id=ID de detalhes do cliente
client.details.additional.info=Informações adicionais
client.details.token.list=Lista de tokens
client.details.refresh-token.list=Lista de atualização de tokens
oauth-client.remove=Excluir
oauth-client.remove.all=Excluir todos
oauth-client=Cliente
oauth-client.token.issue.date=Data de emissão
oauth-client.expires.date=Data de vencimento
oauth-client.issued.tokens=Tokens emitidos
client.details.add=Adicionar cliente OAuth
oauth-client.create=Criar cliente OAuth
oauth-client.id=ID do cliente
oauth-client.secret=Segredo do cliente
oauth-client.redirect.uri=URI de redirecionamento do cliente
oauth-client.access-token.accessTokenValiditySeconds=Acessar validade do token
oauth-client.access-token.refreshTokenValiditySeconds=Atualizar validade do token
oauth-client.access-token.defaultDuration=Usar padrão
oauth-client.title=Título do cliente
oauth-client.description=Descrição
oauth2.error.invalid_client=ID de cliente inválido. Valide o id e parâmetros secretos do cliente.

team.user.enter.email=Digite o email do usuário
user.not.found=Usuário não encontrado
team.profile.update.title=Atualizar as informações da equipe

autocomplete.genus=Encontrar Genus

stats.number-of-countries={0} Países
stats.number-of-institutes={0} Institutos
stats.number-of-accessions={0} Accessões
stats.number-of-historic-accessions={0} Acessões do histórico

navigate.back=Voltar


content.page.list.title=Lista de artigos
article.lang=Idioma
article.classPk=Objeto

session.expiry-warning-title=Aviso de expiração da sessão
session.expiry-warning=Sua sessão atual está prestes a expirar. Deseja estender esta sessão?
session.expiry-extend=Estender sessão

download=Download
download.kml=Fazer download de KML


data-overview=Visão geral estatística
data-overview.intro=Uma visão estetística da informação armazenada no Genesys para os filtros usados - do número total de acessões por país à diversidade das coleções.
data-overview.short=Visão geral
data-overview.institutes=Institutos depositários
data-overview.composition=Composição dos bancos genéticos depositários
data-overview.sources=Fontes de material
data-overview.management=Gerenciamento da coleta
data-overview.availability=Disponibilidade do material
data-overview.otherCount=Outros
data-overview.missingCount=Não especificado
data-overview.totalCount=Total
data-overview.donorCode=FAO WIEWS código do instituto doador
data-overview.mlsStatus=Disponível para distribuição sob o MLS


admin.cache.page.title=Visão geral do cache do aplicativo
cache.stat.map.ownedEntryCount=Entradas de cache proprietárias
cache.stat.map.lockedEntryCount=Entradas cache bloqueadas
cache.stat.map.puts=Número de gravações do cache
cache.stat.map.hits=Número de vistas do cache

loggers.list.page=Lista de agentes configurados
logger.add-logger=Adicionar agente
logger.edit.page=Página de configuração do agente
logger.name=Nome do agente
logger.log-level=Nível de log
logger.appenders=Appenders do log

menu.admin.index=Administrador
menu.admin.loggers=Agentes
menu.admin.caches=Caches
menu.admin.ds2=Conjunto de dados DS2
menu.admin.usermanagement=Gerenciamento do usuário
menu.admin.repository=Repositório
menu.admin.repository.files=Gerenciador de arquivo do repositório
menu.admin.repository.galleries=Galeria de imagens

news.content.page.all.title=Nova lista
news.archive.title=Arquivo de notícias

worldclim.monthly.title=Clima no local da coleta
worldclim.monthly.precipitation.title=Precipitação mensal
worldclim.monthly.temperatures.title=Temperaturas mensais
worldclim.monthly.precipitation=Precipitação mensal [mm]
worldclim.monthly.tempMin=Temperatura mínima [c°]
worldclim.monthly.tempMean=Temperatura média [°C]
worldclim.monthly.tempMax=Temperatura máxima [°C]

worldclim.other-climate=Outros dados climáticos

download.page.title=Antes do seu download
download.download-now=Iniciar download, eu vou esperar.

resolver.page.index.title=Encontrar identificador permanente
resolver.acceNumb=Número da acessão
resolver.instCode=Código do instituto depositário
resolver.lookup=Encontrar identificador
resolver.uuid=UUID
resolver.resolve=Resolver

resolver.page.reverse.title=Resultados de resoluções
accession.purl=URL permanente

menu.admin.kpi=KPI
admin.kpi.index.page=KPI
admin.kpi.execution.page=Execução de KPI
admin.kpi.executionrun.page=Detalhes da execução

accession.pdci=Índice de completude dos dados do Passport
accession.pdci.jumbo=Pontuação PDCI\: {0,number,0.00} de 10.0
accession.pdci.institute-avg=A pontuação média PDCI deste instituto é {0,number,0.00}
accession.pdci.about-link=Leia sobre o índice de completude dos dados Passport
accession.pdci.independent-items=Independentemente do tipo da população
accession.pdci.dependent-items=Dependendo do tipo da população
accession.pdci.stats-text=A pontuação PDCI para {0} acessões é {1,number,0.00}, com pontuação mínima de  {2,number,0.00} e máxima de {3,number,0.00}.

accession.donorNumb=Código do instituto doador
accession.acqDate=Data da aquisição

accession.svalbard-data.url=URL do banco de dados Svalbard
accession.svalbard-data.url-title=Informações de depósito no bando de dados SGSV
accession.svalbard-data.url-text=Exibir informações de depósito SGVS para {0}

filter.download-pdci=Fazer download de dados PDCI
statistics.phenotypic.stats-text=Das {0} acessões, {1} acessões ({2,number,0.##%}) tem pelo menos uma característica registrada em um conjunto de dados disponível no Genesys (em média {3,number,0.##} características em {4,number,0.##} conjunto de dados).

twitter.tweet-this=Tuíte\!
twitter.follow-X=Siga @{0}
linkedin.share-this=Compartilhe no LinkedIn
share.link=Compartilhar link
share.link.placeholder=Aguarde...
share.link.text=Utilize a versão reduzida da URL inteira para esta página\:

welcome.read-more=Leia mais sobre o Genesys.
twitter.latest-on-twitter=As últimas no Twiter
video.play-video=Reproduzir vídeo

heading.general-info=Informações gerais
heading.about=Sobre
heading.see-also=Veja também
see-also.map=Mapa de localização de acessões
see-also.overview=Visão geral sobre a coleta
see-also.country=Mais sobre conservação de PGR em {0}

help.page.intro=Visite a seção de tutoriais para aprender a usar o Genesys.

charts=Gráficos e mapas
charts.intro=Aqui você encontra gráficos e mapas úteis para sua próxima apresentação\!
chart.collections.title=Tamanho das coleções de bancos de genes
chart.attribution-text=www.genesys-pgr.org
chart.collections.series=Número de acessões nos bancos de genes

label.load-more-data=Carregar mais...

userlist.list-my-lists=A lista de acessões foi salva
userlist.title=Título da lista
userlist.description=Descrição da lista
userlist.disconnect=Lista de desconexão
userlist.update-list=Salvar lista atualizada
userlist.make-new-list=Criar nova lista
userlist.shared=Permitir que outros acessem a lista

region.page.list.title = Regiões geográficas FAO
region.page.list.intro = As listas das regiões geográficas FAO abaixo permitem que você acesse os dados sobre acessões coletadas ou mantidas na região.
region.page.show.parent = Exibir região mãe {0}
region.page.show.world = Exibir todas as regiões do mundo
region.countries-in-region=Lista de países em {0}
region.regions-in-region=Regiões FAO em {0}
region.show-all-regions=Listar todas as regiões FAO

repository.file.path=Caminho
repository.file.originalFilename=Nome original do arquivo
repository.file.title=Título
repository.file.subject=Objeto
repository.file.description=Descrição
repository.file.creator=Criador
repository.file.created=Data de criação
repository.file.rightsHolder=Detentor dos direitos
repository.file.accessRights=Direitos de acesso
repository.file.license=Nome ou URL da licença
repository.file.format=Formato
repository.file.contentType=Tipo de conteúdo
repository.file.extension=Extensão do arquivo
repository.file.extent=Extensão
repository.file.bibliographicCitation=Citação bibliográfica
repository.file.dateSubmitted=Data de envio
repository.file.lastModifiedDate=Data da última modificação

file.upload-file=Carregar arquivo
file.upload-file.help=Selecione o arquivo do computador local a ser carregado

repository.gallery=Galeria de imagens
repository.gallery.title=Título da galeria de imagens
repository.gallery.description=Descrição da galeria de imagens
repository.gallery.path=Caminho até a galeria de imagens no repositório\:
repository.gallery.successfully-updated=Atualização da galeria de imagens feita com sucesso\!
repository.gallery.removed=Galeria de imagens removida.
repository.gallery.create-here=Criar galeria de imagens\!
repository.gallery.navigate=Exibir galeria de imagens\!
repository.gallery.downloadImage=Fazer download da imagem

welcome.search-genesys=Pesquisar no Genesys
welcome.networks=Redes

geo.country=País\:
