<%@ tag description="Display filter list" pageEncoding="UTF-8" %>
<%@ tag body-content="scriptless" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="availableFilters" required="true" type="java.util.Map" %>
<%@ attribute name="filters" required="true" type="java.util.Map" %>
<%@ attribute name="appliedFilters" required="true"
              type="org.genesys2.server.service.impl.FilterHandler.AppliedFilters" %>
<%@ attribute name="crops" required="true" type="java.util.Map" %>
<%@ attribute name="crop" required="true" type="org.genesys2.server.model.impl.Crop" %>
<%@ taglib prefix="filters" tagdir="/WEB-INF/tags/filters" %>

<!--Filters-->
<filters:group>
    <filters:panel id="crops" title="filter.crops">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="crops"
                        appliedFilters="${appliedFilters}" type="select" cropList="${crops}"
                        currentCrop="${crop}"/>
    </filters:panel>

    <filters:panel id="cropName" title="filter.cropName">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="cropName"
                        appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="institutecode" title="filter.institute.code">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="institute.code"
                        appliedFilters="${appliedFilters}" type="autocomplete"/>
    </filters:panel>

    <filters:panel id="instcountry" title="filter.institute.country.iso3">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="institute.country.iso3" appliedFilters="${appliedFilters}"
                        type="autocomplete"/>
    </filters:panel>

    <filters:panel id="institutenetworks" title="filter.institute.networks">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="institute.networks" appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="lists" title="filter.lists">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="lists"
                        appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="genus" title="filter.taxonomy.genus">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="taxonomy.genus"
                        appliedFilters="${appliedFilters}" type="autocomplete"/>
    </filters:panel>

    <filters:panel id="species" title="filter.taxonomy.species">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="taxonomy.species" appliedFilters="${appliedFilters}"
                        type="autocomplete"/>
    </filters:panel>

    <filters:panel id="subtaxa" title="filter.taxonomy.subtaxa">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="taxonomy.subtaxa" appliedFilters="${appliedFilters}"
                        type="autocomplete"/>
    </filters:panel>

    <filters:panel id="sciname" title="filter.taxonomy.sciName">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="taxonomy.sciName" appliedFilters="${appliedFilters}"
                        type="autocomplete"/>
    </filters:panel>

    <filters:panel id="acceNumb" title="filter.acceNumb">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="acceNumb"
                        appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="seqNo" title="filter.seqNo">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="seqNo"
                        appliedFilters="${appliedFilters}" type="range"/>
    </filters:panel>

    <filters:panel id="alias" title="filter.alias">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="alias"
                        appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="sampstat" title="filter.sampStat">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="sampStat"
                        appliedFilters="${appliedFilters}" type="option"/>
    </filters:panel>

    <filters:panel id="origcty" title="filter.orgCty.iso3">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="orgCty.iso3"
                        appliedFilters="${appliedFilters}" type="autocomplete"/>
    </filters:panel>

    <filters:panel id="geolatitude" title="filter.geo.latitude">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="geo.latitude"
                        appliedFilters="${appliedFilters}" type="range"/>
    </filters:panel>

    <filters:panel id="sgsv" title="filter.sgsv">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="sgsv"
                        appliedFilters="${appliedFilters}" type="boolean"/>
    </filters:panel>

    <filters:panel id="mlsStatus" title="filter.mlsStatus">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="mlsStatus"
                        appliedFilters="${appliedFilters}" type="boolean"/>
    </filters:panel>

    <filters:panel id="art15" title="filter.art15">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="art15"
                        appliedFilters="${appliedFilters}" type="boolean"/>
    </filters:panel>

    <filters:panel id="available" title="filter.available">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="available"
                        appliedFilters="${appliedFilters}" type="boolean"/>
    </filters:panel>

    <filters:panel id="historic" title="filter.historic">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="historic"
                        appliedFilters="${appliedFilters}" type="boolean"/>
    </filters:panel>

    <filters:panel id="collMissId" title="filter.coll.collMissId">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}"
                        filterKey="coll.collMissId" appliedFilters="${appliedFilters}" type="like"/>
    </filters:panel>

    <filters:panel id="storage" title="filter.storage">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="storage"
                        appliedFilters="${appliedFilters}" type="option"/>
    </filters:panel>

    <filters:panel id="pdci" title="filter.pdci">
        <filters:filter availableFilters="${availableFilters}" filterMap="${filters}" filterKey="pdci"
                        appliedFilters="${appliedFilters}" type="range"/>
    </filters:panel>
</filters:group>