<%@ tag description="HTML headers" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ attribute name="title" required="false" type="java.lang.String" description="title" %>
<%@ attribute name="description" required="false" type="java.lang.String" description="description" %>
<%@ attribute name="keywords" required="false" type="java.lang.String" description="keywords" %>
<%@ attribute name="imageUrl" required="false" type="java.lang.String" description="URL to an image" %>

<%-- Twitter --%>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@GenesysPGR" />
<meta name="twitter:title" content="<c:out value="${title}" />" />

<c:if test="${description ne null}">
	<meta name="description" content="<c:out value="${description}" />" />
	<meta name="twitter:description" content="<c:out value="${description}" />" />
</c:if>
<c:if test="${keywords ne null}">
	<meta name="keywords" content="<c:out value="${keywords}" />" />
</c:if>

<c:if test="${imageUrl ne null}">
	<meta name="twitter:image" content="<c:out value="${imageUrl}" />" />
</c:if>
