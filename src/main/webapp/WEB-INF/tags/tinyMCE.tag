<%@ tag description="Make textarea into tinyMCE" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="selector" required="true" type="java.lang.String" %>
<%@ attribute name="menubar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="statusbar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="directionality" required="false" type="java.lang.String" %>
<%
	if (menubar == null) {
		menubar = false;
	}
	if (statusbar == null) {
		statusbar = false;
	}
	if (height == null) {
		height = 200;
	}
	if (directionality == null) {
		//document.getElementById('editForm').dir
		directionality = "document.dir";
	}
%>
jQuery(document).ready(function () {
  tinyMCE.init({
    selector: '<%= selector %>',
    menubar: <%= menubar %>,
    toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | cleanup removeformat code',
    statusbar: <%= statusbar %>,
    height: <%= height %>,
    plugins: 'link autolink code',
    directionality: <%= directionality %>,
    convert_urls: false,
    document_base_url: '<c:url value="/" />'
  });
});
