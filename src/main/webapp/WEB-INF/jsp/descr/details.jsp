<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="metadata.page.title" /></title>
</head>
<body>

	<div class="informative-h1 green-bg row">
		<div class="col-md-12 col-sm-12">
			<h1>
				<c:out value="${trait.getTitle(pageContext.response.locale)}" />

			</h1>
			<p><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></p>
		</div>
	</div>
	<div class="desc-content">
		<div class="applied-filters">
			<h4><spring:message code="filter.crop" />: <b><a href="<c:url value="/c/${trait.crop.shortName}/descriptors" />"><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></a></b></h4>
		</div>
		<div class="selected-category">
			<h4><spring:message code="descriptor.category" />: <b><c:out value="${trait.category.getName(pageContext.response.locale)}" /></b></h4>
		</div>
		<h3 class="list-title">
			<spring:message code="ce.methods" />
		</h3>

			<table class="accessions">
				<thead>
				<tr>
					<td class="idx-col">&nbsp;</td>
					<td><p><spring:message code="ce.trait" /></p></td>
					<td><p><spring:message code="ce.sameAs" /></p></td>
					<td><p><spring:message code="ce.method" /></p></td>
					<td><p><spring:message code="unit-of-measure" /></p></td>
					<td><p><spring:message code="method.fieldName" /></p></td>
				</tr>
				</thead>
				<tbody>
				<c:forEach items="${traitMethods}" var="method">
					<tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<td>&nbsp;</td>
						<td><c:out value="${method.parameter.getTitle(pageContext.response.locale)}" /></td>
						<td>
							<c:if test="${method.parameter.rdfUri ne null}">
								<a href="<c:url value="${method.parameter.rdfUri}" />">
									<c:out value="${method.parameter.getRdfUriId()}" />
								</a>
							</c:if>
						</td>
						<td><a href="<c:url value="/descriptors/${trait.id}/${method.id}" />"><c:out value="${method.getMethod(pageContext.response.locale)}" /></a></td>
						<td><c:out value="${method.unit}" /></td>
						<td><c:out value="${method.fieldName}" /></td>
					</tr>
				</c:forEach>
				</tbody>
			</table>

	</div>

</body>
</html>