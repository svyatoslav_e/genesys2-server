<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="traits.page.title" /></title>
</head>
<body>
	<div class="informative-h1 green-bg row">
		<div class="col-md-12 col-sm-12">
			<h1 class="descr">
				<spring:message code="trait-list" />
			</h1>
		</div>
	</div>


	<div class="main-col-header clearfix">
	<div class="nav-header pull-left">
		<local:paginate2 page="${pagedData}" />
	</div>
	</div>
	
	<c:if test="${crop ne null}">
		<div class="applied-filters">
			<spring:message code="filter.crop" />: <b><a href="<c:url value="/c/${crop.shortName}" />"><c:out value="${crop.getName(pageContext.response.locale)}" /></a></b>
		</div>
	</c:if>

	<table class="accessions">
		<thead>
			<tr>
				<td class="idx-col">&nbsp;</td>
				<td><p>Trait</p></td>
				<td><p>Crop</p></td>
				<td><p><spring:message code="descriptor.category" /></p></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${pagedData.content}" var="trait" varStatus="status">
				<tr class="${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col"><c:out value="${status.count + pagedData.size * pagedData.number}" /></td>
					<td><a href="<c:url value="/descriptors/${trait.id}" />"><c:out value="${trait.getTitle(pageContext.response.locale)}" /></a></td>
					<td><a href="<c:url value="/c/${trait.crop.shortName}" />"><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></a></td>
					<td><c:out value="${trait.category.getName(pageContext.response.locale)}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>