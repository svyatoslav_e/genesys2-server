<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="metadata.page.title" /></title>
<style type="text/css">
	span.method-code { font-weight: bold; font-size: 110%; margin-left: 2em; float: right; } 
</style>
</head>
<body>
	<div class="informative-h1 green-bg row">
		<div class="col-md-12 col-sm-12">
			<h1>
				<c:out value="${trait.title}" />
			</h1>
		</div>
	</div>
	<div class="desc-content">
		<div class="applied-filters">
			<h4>
				<spring:message code="filter.crop" />:
				<b><a href="<c:url value="/c/${trait.crop.shortName}/descriptors" />"><c:out value="${trait.crop.getName(pageContext.response.locale)}" /></a></b>
			</h4>
		</div>

			<table class="accessions">
				<thead>
				<tr class="acn ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<td class="idx-col">&nbsp;</td>
					<td><p><spring:message code="ce.trait" /></p></td>
					<td><p><spring:message code="ce.method" /></p></td>
					<td><p><spring:message code="ce.sameAs" /></p></td>
					<td><p><spring:message code="method.fieldName" /></p></td>
					<td><p><spring:message code="unit-of-measure" /></p></td>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>&nbsp;</td>
					<td><a href="<c:url value="/descriptors/${trait.id}" />"><c:out value="${method.parameter.getTitle(pageContext.response.locale)}" /></a></td>
					<td><a href="<c:url value="/descriptors/${trait.id}/${method.id}" />"><c:out value="${method.getMethod(pageContext.response.locale)}" /></a></td>
					<td>
						<c:if test="${method.rdfUri ne null}">
							<a href="<c:url value="${method.rdfUri}" />">
								<c:out value="${method.getRdfUriId()}" />
							</a>
						</c:if>
					</td>
					<td><c:out value="${method.fieldName}" /></td>
					<td><c:out value="${method.unit}" /></td>
				</tr>
				</tbody>
			</table>



		<c:if test="${method.coded}">

			<%--<ul class="funny-list">--%>
			<%--<c:forEach items="${codeMap.keySet()}" var="key" varStatus="status">--%>
			<%--<li class="${status.count % 2 == 0 ? 'even' : 'odd'}">--%>
			<%--<span class="method-code"><c:out value="${key}" /></span>--%>
			<%--<a title="<spring:message code="view.accessions" />" href="<c:url value="/explore"><c:param name="crop" value="${trait.crop.shortName}" /><c:param name="filter">{"gm:${method.id}":["${key}"]}</c:param></c:url>">--%>
			<%--<c:out value="${codeMap[key]}" />--%>
			<%--</a>--%>
			<%--<c:out value="count=${codeStatistics[key]}" />--%>
			<%--</li>--%>
			<%--</c:forEach>--%>
			<%--</ul>--%>

			<div class="row institute-list" dir="ltr">
				<h3 class="list-title"><spring:message code="method.coding-table" /></h3>
				<c:forEach items="${codeMap.keySet()}" var="key" varStatus="status">
					<div class="col-sm-12 col-xs-12 clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
						<span class="method-code"><c:out value="${key}" /></span>
						<a title="<spring:message code="view.accessions" />" href="<c:url value="/explore"><c:param name="crop" value="${trait.crop.shortName}" /><c:param name="filter">{"gm:${method.id}":["${key}"]}</c:param></c:url>">
						<c:out value="${codeMap[key]}" />
						</a>
						<div class="pull-right">
							<c:out value="count=${codeStatistics[key]}" />
						</div>
					</div>
				</c:forEach>
			</div>
		</c:if>



		<%--<ul class="funny-list">--%>
		<%--<c:forEach items="${metadatas}" var="metadata" varStatus="status">--%>
		<%--<li class="clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">--%>
		<%--<a class="show pull-left" href="<c:out value="/data/view/${metadata.id}" />"><c:out value="${metadata.title}" /></a>--%>
		<%--<div class="pull-right">--%>
		<%--<c:out value="${metadata.instituteCode}" />--%>
		<%--</div></li>--%>
		<%--</c:forEach>--%>
		<%--</ul>--%>

		<div class="row institute-list" dir="ltr">
			<h3 class="list-title"><spring:message code="accession.metadatas" /></h3>
			<c:forEach items="${metadatas}" var="metadata" varStatus="status">
				<div class="col-sm-12 col-xs-12 clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
					<a class="show pull-left" href="<c:out value="/data/view/${metadata.id}" />"><c:out value="${metadata.title}" /></a>
					<div class="pull-right">
						<c:out value="${metadata.instituteCode}" />
					</div>
				</div>
			</c:forEach>
		</div>

	</div>

</body>
</html>