<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="project.page.list.title" /></title>
</head>
<body class="news-page selected-list">
	<h1 class="green-bg">
		<spring:message code="project.page.list.title" />
	</h1>

	<div class="row main-col-header clearfix">
		<div class="nav-header">
			<local:paginate2 page="${pagedData}"/>
		</div>
	</div>
	<security:authorize access="hasRole('ADMINISTRATOR') or hasRole('CONTENTMANAGER') or hasRole('VETTEDUSER')">
		<a href="<c:url value="/project/add-project" />" class="close">
			<spring:message code="add" />
		</a>
	</security:authorize>

	<div class="row institute-list">
		<c:forEach items="${pagedData.content}" var="project" varStatus="status">
			<div class="col-sm-12 col-xs-12 clearfix ${status.count % 2 == 0 ? 'even' : 'odd'}">
				<a class="show pull-left" href="<c:url value="/project/${project.code}" />">
					<b>
						<c:out value="${project.code}" />
					</b>
					<c:out value="${project.name}" />
				</a>
			</div>
		</c:forEach>
	</div>

</body>
</html>
