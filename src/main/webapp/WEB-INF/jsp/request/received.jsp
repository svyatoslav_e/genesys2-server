<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body>
	<h1>
		<spring:message code="request.page.title" />
	</h1>

	<cms:blurb blurb="${blurp}" />

</body>
</html>