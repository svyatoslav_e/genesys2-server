<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="request.page.title" /></title>
</head>
<body class="selected-list">
	<h1 class="green-bg">
		<spring:message code="request.page.title" />
	</h1>

	<gui:alert type="info" display="${pagedData == null}">
		<spring:message code="selection.empty-list-warning" />
	</gui:alert>

	<c:if test="${pagedData != null}">

		<div class="row main-col-header clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav-header clearfix">
				<filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
				<%--<div class="nav-header pull-left">
					<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" /></div>
					<div class="pagination">
						<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
						<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
					</div>
				</div>--%>
			</div>
		</div>
	
	
		<gui:alert type="info">
			<spring:message code="request.total-vs-available" arguments="${totalCount},${availableCount}" />
		</gui:alert>

		<cms:blurb blurb="${blurp}" />

	<div class="row accessions-table-wrapper">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<table class="accessions">
			<thead>
				<tr>
					<td class="idx-col"><p></p></td>
					<td><p></p></td>
					<td><p><spring:message code="accession.availability" /></p></td>
					<td><p><spring:message code="accession.historic" /></p></td>
					<td><p><spring:message code="accession.accessionName" /></p></td>
					<td class="notimportant"><p><spring:message code="accession.taxonomy" /></p></td>
					<%-- <td class="notimportant"><spring:message code="accession.origin" /></td> --%>
					<td><p><spring:message code="accession.holdingInstitute" /></p></td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
					<tr id="a${accession.id}" class="acn targeted ${status.count % 2 == 0 ? 'even' : 'odd'} ${accession.availability!=false and accession.institute.allowMaterialRequests ? '' : 'not-available'} ${accession.historic ? 'historic-record' : ''}">
						<td class="idx-col"><p><c:out value="${status.count + pagedData.size * pagedData.number}" /></p></td>
						<td class="sel ${selection.containsId(accession.id) ? 'picked' : ''}" x-aid="${accession.id}"></td>
						<td><p><spring:message code="accession.availability.${accession.availability}" /></p></td>
						<td><p><spring:message code="accession.historic.${accession.historic}" /></p></td>
						<td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></p></td>
						<td class="notimportant"><p><c:out value="${accession.taxonomy.taxonName}" /></p></td>
						<%-- <td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td> --%>
						<td class="notimportant"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></p></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

			<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
			<table class="accessions mobile-accessions">
				<thead>
				<tr>
					<td class="idx-col"><p><c:out value="${status.count + pagedData.size * pagedData.number}" /></p></td>
				</tr>
				</thead>
				<tbody>
					<tr>
						<td><p><spring:message code="accession.availability" /></p></td>
						<td><p><spring:message code="accession.availability.${accession.availability}" /></p></td>
					</tr>
					<tr>
						<td><p><spring:message code="accession.historic" /></p></td>
						<td><p><spring:message code="accession.historic.${accession.historic}" /></p></td>
					</tr>
					<tr>
						<td><p><spring:message code="accession.accessionName" /></p></td>
						<td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></p></td>
					</tr>
					<tr>
						<td class="notimportant"><p><spring:message code="accession.taxonomy" /></p></td>
						<td class="notimportant"><p><c:out value="${accession.taxonomy.taxonName}" /></p></td>
					</tr>
					<%--<tr>
						<td class="notimportant"><spring:message code="accession.origin" /></td>
						<td class="notimportant"><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></td>
					</tr>--%>
					<tr>
						<td><p><spring:message code="accession.holdingInstitute" /></p></td>
						<td class="notimportant"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></p></td>
					</tr>
				</tbody>
			</table>
			</c:forEach>
		</div>
	</div>

		<c:if test="${availableCount gt 0}">
			<form method="post" action="<c:url value="/request/start" />" class="form-horizontal">
				<div class="form-actions">
					<input class="btn btn-primary" type="submit" value="<spring:message code="request.start-request" />" />
				</div>
                <!-- CSRF protection -->
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			</form>
		</c:if>

	</c:if>
</body>
</html>