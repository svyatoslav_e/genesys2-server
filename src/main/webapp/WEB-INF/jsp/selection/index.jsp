<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="selection.page.title" /></title>
</head>
<body class="selected-list">
	<cms:informative-h1 title="selection.page.title" fancy="true" info="selection.page.intro" />

	<gui:alert type="warning" display="${resultFromSave ne null}">
		<spring:message code="${resultFromSave}" />
	</gui:alert>

	<c:if test="${pagedData == null}">
	    <gui:alert type="info">
	        <spring:message code="selection.empty-list-warning"/>
	    </gui:alert>
	    
	    <security:authorize access="isAuthenticated()">
	    	<c:if test="${fn:length(userAccessionLists) gt 0}">
			<div class="">
				<spring:message code="userlist.list-my-lists" /> 
		        <ul class="funny-list" role="" id="user-accession-lists">
		            <c:forEach items="${userAccessionLists}" var="userList">
		                <li><a href="/sel/load?list=${userList.uuid}"><c:out value="${userList.title}" /></a>
		                <div class="pull-right"><c:out value="${userList.description}" /></div>
		                </li>
		            </c:forEach>
		        </ul>
			</div>
			</c:if>
		</security:authorize>
	</c:if>

<c:if test="${pagedData != null}">

 		<div class="row main-col-header clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nav-header clearfix">
				<%--<div class="results"><spring:message code="accessions.number" arguments="${pagedData.totalElements}" /></div>--%>
					<div class="pull-right list-view-controls">
						<form method="post" action="<c:url value="/sel/dwca" />">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-save"></span><span><spring:message code="filter.download-dwca" /></span></button>
						</form>
						<form method="post" action="<c:url value="/sel/download/mcpd" />">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
							<button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-save"></span><span><spring:message code="filter.download-mcpd" /></span></button>
						</form>
					</div>
					<%--<div class="col-sm-12 col-md-6">
						<div class="pagination">
							<spring:message code="paged.pageOfPages" arguments="${pagedData.number+1},${pagedData.totalPages}" />
							<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number eq 0 ? 1 : pagedData.number}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.previous-page" /></a>
							<a href="<spring:url value=""><spring:param name="page" value="${pagedData.number+2}" /><spring:param name="filter" value="${jsonFilter}" /></spring:url>"><spring:message code="pagination.next-page" /></a>
						</div>
					</div>--%>
					<!--Pagination-->
					<filters:pagination pagedData="${pagedData}" jsonFilter="${jsonFilter}"/>
			</div>
		</div>
		<div class="row accessions-table-wrapper">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table class="accessions">
					<thead>
						<tr>
							<td class="idx-col"><p></p></td>
							<td><p></p></td>
							<td><p><spring:message code="accession.accessionName" /></p></td>
							<td><p><spring:message code="accession.taxonomy" /></p></td>
							<td class="notimportant"><p><spring:message code="accession.origin" /></p></td>
							<td class="notimportant"><p><spring:message code="accession.holdingInstitute" /></p></td>
							<%-- <td class="notimportant"><spring:message code="accession.holdingCountry" /></td> --%>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
							<tr id="a${accession.id}" class="acn targeted ${accession.historic ? 'historic-record' : ''} ${status.count % 2 == 0 ? 'even' : 'odd'}">
								<td class="idx-col"><p><c:out value="${status.count + pagedData.size * pagedData.number}" /></p></td>
								<td class="sel ${selection.containsId(accession.id) ? 'picked' : ''}" x-aid="${accession.id}"></td>
								<td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></p></td>
								<td><p><c:out value="${accession.taxonomy.taxonName}" /></p></td>
								<td class="notimportant"><p><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></p></td>
								<td class="notimportant"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></p></td>
								<%--<td class="notimportant"><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td>--%>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<c:forEach items="${pagedData.content}" var="accession" varStatus="status">
				<table class="accessions mobile-accessions">
					<thead>
						<td class="idx-col"><p><c:out value="${status.count + pagedData.size * pagedData.number}" /></p></td>
					</thead>
					<tbody>
						<tr>
							<td><p><spring:message code="accession.accessionName" /></p></td>
							<td><p><a href="<c:url value="/acn/id/${accession.id}" />"><b><c:out value="${accession.accessionName}" /></b></a></p></td>
						</tr>
					<tr>
						<td><p><spring:message code="accession.taxonomy" /></p></td>
						<td><p><c:out value="${accession.taxonomy.taxonName}" /></p></td>
					</tr>
					<tr>
						<td class="notimportant"><p><spring:message code="accession.origin" /></p></td>
						<td class="notimportant"><p><c:out value="${accession.countryOfOrigin.getName(pageContext.response.locale)}" /></p></td>
					</tr>
					<tr>
						<td class="notimportant"><p><spring:message code="accession.holdingInstitute" /></p></td>
						<td class="notimportant"><p><a href="<c:url value="/wiews/${accession.institute.code}" />"><c:out value="${accession.institute.code}" /></a></p></td>
					</tr>
					<%--<tr>
						<td class="notimportant"><spring:message code="accession.holdingCountry" /></td>
						<td class="notimportant"><a href="<c:url value="/geo/${accession.institute.country.code3}" />"><c:out value="${accession.institute.country.getName(pageContext.response.locale)}" /></a></td>
					</tr>--%>
					</tbody>
				</table>
				</c:forEach>
			</div>
			</div>
			<div class="row main-col-header form-actions-wrapper">
				<form method="post" action="<c:url value="/sel/order" />" class="form-vertical col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="form-actions">
						<a href="<c:url value="/sel/" />"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-refresh"></span><span><spring:message code="selection.reload-list" /></span></button></a>
						<button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-send"></span><span><spring:message code="selection.send-request" /></span></button>
						<a href="<c:url value="/sel/clear" />"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-erase"></span><span><spring:message code="selection.clear" /></span></button></a>
						<a href="<c:url value="/sel/map" />"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-globe"></span><span><spring:message code="selection.map" /></span></button></a>
					</div>
				  <!-- CSRF protection -->
				  <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</div>

		</c:if>

		<div class="tabs-wrapper">
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#tab-add-many" aria-controls="tab-add-many" role="tab" data-toggle="tab"><spring:message code="selection.add-many" /></a></li>
			<security:authorize access="isAuthenticated()">
			<li role="presentation"><a href="#tab-manage-list" aria-controls="tab-manage-list" role="tab" data-toggle="tab"><spring:message code="user.accession.list.title" /></a></li>
			</security:authorize>
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="tab-add-many">
				<form method="post" action="<c:url value="/sel/add-many" />" class="form-horizontal">
					<div class="form-group">
						<label for="accessionIds" class="control-label"><spring:message code="selection.add-many.accessionIds" /></label>
						<div class="controls">
							<textarea class="form-control" placeholder="IG 523121, TMB-1" name="accessionIds"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="accessionIds" class="control-label"><spring:message code="selection.add-many.instCode" /></label>
						<div class="controls">
							<input class="form-control" placeholder="XXX000" name="instCode" />
						</div>
					</div>
					<div class="form-group transparent">
						<input type="submit" class="btn btn-primary" value="<spring:message code="selection.add-many.button" />" />
					</div>
					<!-- CSRF protection -->
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</div>

			<security:authorize access="isAuthenticated()">
			<div role="tabpanel" class="tab-pane" id="tab-manage-list">
				<form method="post" action="<c:url value="/sel/userList"/>" role="form" class="form-horizontal">
					<div class="form-group transparent">
						<div class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><spring:message code="userlist.list-my-lists" /> <b class="caret"></b></a>
							<ul class="dropdown-menu" role="menu" id="user-accession-lists">
								<c:forEach items="${userAccessionLists}" var="userList">
								<li><a href="/sel/load?list=${userList.uuid}"><c:out value="${userList.title}" /></a></li>
								</c:forEach>
							</ul>
						</div>
					</div>
				<div class="form-group">
					<label for="accessionListTitle" class="control-label"><spring:message code="userlist.title" /></label>
					<input id="accessionListTitle" type="text" name="title" class="form-control" value="${selection.userAccessionList.title}"/>
				</div>
				<div class="form-group">
					<label for="accessionListShared" class="control-label">
						<input type="checkbox" id="accessionListShared" name="shared" value="true" ${selection.userAccessionList.shared ? "checked" : ""}/>
					<spring:message code="userlist.shared" /></label>
				</div>
				<div class="form-group">
					<label for="accessionListDescr" class="control-label"><spring:message code="userlist.description" /></label>
					<textarea name="description" rows="3" id="accessionListDescr" class="form-control"><c:out value="${selection.userAccessionList.description}"/></textarea>
				</div>
				<div class="form-group transparent">
					<c:if test="${selection.userAccessionList.uuid ne null}">
						<input type="hidden" name="uuid" value="${selection.userAccessionList.uuid}"/>
						<input type="submit" class="btn btn-primary" value="<spring:message code="userlist.update-list"/>" name="update" />
						<input type="submit" class="btn btn-default" value="<spring:message code="userlist.disconnect"/>" name="disconnect" />
						<input type="submit" class="btn btn-default" value="<spring:message code="delete"/>" name="delete" />

					  <security:authorize access="hasRole('ADMINISTRATOR') or hasPermission(#selection.userAccessionList, 'ADMINISTRATION')">
						  <a class="btn btn-default" href="<c:url value="/acl/org.genesys2.server.model.impl.AccessionList/${selection.userAccessionList.id}/permissions"><c:param name="back"><c:url value="/sel" /></c:param></c:url>">
							  <spring:message code="edit-acl"/>
						  </a>
					  </security:authorize>
					</c:if>
					<input type="submit" class="btn btn-primary" value="<spring:message code="userlist.make-new-list"/>" name="save"/>
				</div>
				<!-- CSRF protection -->
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				  </form>
			</div>
			</security:authorize>
		  </div>
		</div>
	
</body>
</html>