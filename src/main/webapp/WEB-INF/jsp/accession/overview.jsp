<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title><spring:message code="data-overview"/></title>
    <script type="text/javascript" src="<c:url value="/html/1/js/browse.js" />"></script>
    <script type="text/javascript" src="<c:url value="/explore/i18n.js"/>"></script>
</head>
<body class="overview-page explore-page">
<%--<body class="explore-page">--%>
<cms:informative-h1 title="data-overview" fancy="true" info="data-overview.intro"/>

<div class="container-fluid">

    <div id="loading-popup-id" class="loading-popup">
        <div class="loading-popup-content">
            <p><spring:message code="prompt.loading-data"/></p>
        </div>
    </div>

    <div id="error-loading-popup-id" class="error-loading-popup">
        <div class="error-loading-popup-content">
            <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
            <p><spring:message code="prompt.loading-data-failed"/></p>
        </div>
    </div>

    <div class="">
        <%-- filter list --%>
        <filters:filter-list availableFilters="${availableFilters}" filters="${filters}"
                             appliedFilters="${appliedFilters}" crops="${crops}" crop="${crop}"/>

        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-12 main-col-header">
            <div class="nav-header clearfix">

                <div class="pull-right list-view-controls">

                    <div class="btn-group" id="shareLink">
                        <a type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false" id="menuShareLink">
                            <span class="glyphicon glyphicon-share"></span>
                            <span><spring:message code="share.link"/></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="padding10">
                                <p><spring:message code="share.link.text"/></p>
                                <input id="shortLink" type="text"
                                       placeholder="<spring:message code="share.link.placeholder" />" value=""/>
                            </li>
                        </ul>
                    </div>

                    <a class="btn btn-default" id="exploreLink"
                       href="<c:url value="/explore"><c:param name="filter" value="${jsonFilter}" /></c:url>">
                        <span class="glyphicon glyphicon-list"></span>
                        <span style="margin-left: 0.5em;"><spring:message code="view.accessions"/></span></a>
                    <a class="btn btn-default" id="mapLink"
                       href="<c:url value="/explore/map"><c:param name="filter" value="${jsonFilter}" /></c:url>">
                        <span class="glyphicon glyphicon-globe"></span>
                        <span style="margin-left: 0.5em;"><spring:message code="maps.view-map"/></span></a>
                </div>

                <div class="pull-left list-view-controls">
                    <a class="btn btn-default btn-back" href="">
                        <spring:message code="navigate.back"/></a>
                </div>

            </div>

        </div>

        <div id="content-area" class="col-lg-10 col-md-9 col-sm-9 col-xs-12">
            <div id="mode-content">
                <h3 class="row row-section-heading">
                    <span class="glyphicon glyphicon-leaf"></span>
                    <spring:message code="data-overview.institutes"/>
                </h3>
                <overview:group-row>
                    <overview:row termResult="${statsInstCode}" type="instCode"
                                  messageCode="filter.institute.code"/>
                    <overview:row termResult="${statsInstCountry}" type="country"
                                  messageCode="filter.institute.country.iso3"/>
                </overview:group-row>

                <h3 class="row row-section-heading"><span class="glyphicon glyphicon-leaf"></span>
                    <spring:message code="data-overview.composition"/>
                </h3>
                <overview:group-row>
                    <overview:row termResult="${statsCrops}" type="crop" messageCode="filter.crops"/>
                    <overview:row termResult="${statsSampStat}" type="i18n.accession.sampleStatus"
                                  messageCode="filter.sampStat"/>
                </overview:group-row>
                <overview:group-row>
                    <overview:row termResult="${statsGenus}" type="genus" messageCode="filter.taxonomy.genus"/>
                    <overview:row termResult="${statsSpecies}" type="species"
                                  messageCode="filter.taxonomy.species"/>
                </overview:group-row>

                <h3 class="row row-section-heading"><span class="glyphicon glyphicon-leaf"></span>
                    <spring:message code="data-overview.sources"/>
                </h3>
                <overview:group-row>
                    <overview:row termResult="${statsOrgCty}" type="country" messageCode="filter.orgCty.iso3"/>
                    <overview:row termResult="${statsDonorCode}" type="instCode"
                                  messageCode="data-overview.donorCode"/>
                </overview:group-row>

                <h3 class="row row-section-heading"><span class="glyphicon glyphicon-leaf"></span>
                    <spring:message code="data-overview.availability"/>
                </h3>
                <overview:group-row>
                    <overview:row termResult="${statsMLS}" type="bool" messageCode="data-overview.mlsStatus"/>
                    <overview:row termResult="${statsAvailable}" type="bool" messageCode="filter.available"/>
                    <overview:row termResult="${statsHistoric}" type="bool" messageCode="filter.historic"/>
                </overview:group-row>

                <h3 class="row row-section-heading"><span class="glyphicon glyphicon-leaf"></span>
                    <spring:message code="data-overview.management"/>
                </h3>
                <overview:group-row>
                    <overview:row termResult="${statsDuplSite}" type="instCode" count="${accessionCount}"
                                  messageCode="filter.duplSite"/>
                    <overview:row termResult="${statsSGSV}" type="bool" messageCode="filter.sgsv"/>
                </overview:group-row>

                <overview:group-row>
                    <overview:row termResult="${statsStorage}" type="i18n.accession.storage"
                                  count="${accessionCount}"
                                  messageCode="filter.storage"/>
                </overview:group-row>
            </div>
        </div>
    </div>
</div>

<content tag="javascript">
    <script type="text/javascript">
        var jsonData = ${jsonFilter};
        localStorage.setItem("historyStep", 1);

        $(document).ready(function () {
            BrowseUtil.applySuggestions(jsonData, messages);

            $(document.getElementsByClassName("btn btn-default btn-back")).on('click', function (event) {
                event.preventDefault();
                var stepValue = localStorage.getItem("historyStep");
                window.history.go(-stepValue);
            });

            $.each($('#collapseFilters').find("div.panel-collapse"), function () {
                <c:forEach items="${appliedFilters}" var="appliedFilter">
                var collapseDiv = $(this).find($("div[id*='${appliedFilter.key}']"));
                if (collapseDiv[0] !== undefined) {
                    $(this).collapse("show");
                }
                </c:forEach>
            });

            $(window).on("popstate", function(e) {
                if (e.originalEvent.state !== null) {
                    jsonData = JSON.parse(decodeURIComponent((new RegExp('[?|&]filter=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20'))) || [];
                    page = decodeURIComponent((new RegExp('[?|&]page=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 1;
                    results = decodeURIComponent((new RegExp('[?|&]results=' + '([^&;]+?)(&|#|;|$)').exec(location.search)
                            || [null, ''])[1].replace(/\+/g, '%20')) || 50;
                    var filter = JSON.stringify(jsonData);
                    var requestUrl = '//' + location.host + location.pathname + "/json" +
                            "?filter=" + encodeURIComponent(filter);
                    $.ajax({
                        url: requestUrl,
                        method: 'get',
                        success: function (response) {
                            renderData(response);
                        }
                    });
                    BrowseUtil.applySuggestions(jsonData, messages);
                } else {
                    location.reload();
                }
            });

            GenesysFilterUtil.registerAutocomplete(".filters", jsonData);

            $("body").on("keypress", ".string-type", function (e) {
                if (e.keyCode == 13) {
                    console.log("press")

                    var btn = $(this).parent().find("button");
                    console.log("1")
                    var selectedValue = $(this).parent().parent().find(".like-switcher option:selected").val();
                    console.log("2")
                    if (selectedValue == "like") {
                        GenesysFilter.filterLike(btn, jsonData, BrowseUtil.i18nFilterMessage);
                    } else {
                        console.log("btn", btn)
                        console.log("jsonData", jsonData)
                        console.log("03")
                        GenesysFilter.filterAutocomplete(btn, jsonData);
                        console.log("04")
                    }
                }
            });

            $("body").on("click", ".filter-auto", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }

                var selectedValue = $(this).parent().parent().parent().find(".like-switcher option:selected").val();
                if (selectedValue == "like") {
                    GenesysFilter.filterLike($(this), jsonData, BrowseUtil.i18nFilterMessage);
                } else {
                    GenesysFilter.filterAutocomplete($(this), jsonData);
                }
            });

            $("body").on("click", ".filter-list", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                var text = $(this).parent().text();
                text = text.substring(0, text.indexOf('('));
                GenesysFilter.filterList($(this), jsonData, text);
            });

            $("body").on("click", ".filter-range", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterRange($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $("body").on("click", ".filter-bool", function () {
                if (jsonData[$(this).attr("i-key")] === undefined) {
                    BrowseUtil.enableFilter(this, jsonData);
                }
                GenesysFilter.filterBoolean($(this), jsonData, BrowseUtil.i18nFilterMessage);
            });

            $('body').on('click', '.filter-crop', function () {
                GenesysFilter.filterCrop($(this), jsonData);
            });

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();
                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $("body").on("change", "#more-filters", function () {
                var key = $(this).val();
                var normKey = GenesysFilterUtil.normKey(key);
                var input = $(this).parent().find("input[type='text']");
                input.attr('id', normKey + "_input");
                if (key == "institute.country.iso3") {
                    input.attr('x-source', "/explore/ac/orgCty.iso3");
                } else {
                    input.attr('x-source', "/explore/ac/" + key);
                }

                var btn = $(this).parent().find("button.filter-auto");
                btn.attr("norm-key", normKey);
                btn.attr("i-key", key);
                jsonData[key] = [];
            });

            $("body").on("click", ".filtval", function (event) {
                event.preventDefault();
                var key = $(this).attr("i-key");
                var normKey = GenesysFilter.normKey(key);
                var value = $(this).attr("x-key").replace(normKey, "");

                if (value == "null") value = null;
                GenesysFilterUtil.removeValue(value, key, jsonData);
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);

                $(this).remove();
                $('input[i-key=" + normKey + "][value=" + value + "]').prop('checked', false);
            });

            $("#menuShareLink").on("click", function () {
                if ($('#shortLink').val() === '') {
                    $.ajax({
                        type: 'POST',
                        url: '/explore/shorten-url',
                        data: {
                            'browser': "overview", 'filter': JSON.stringify(jsonData)
                        },
                        success: function (response) {
                            var inp = $("#shortLink");
                            inp.val(response.shortUrl);
                            inp.select();
                        }
                    });
                } else {
                    console.log('No repeat.');
                }
            });

            $("body").on("click", ".applyBtn", function () {
                applyFilters();
                BrowseUtil.applySuggestions(jsonData, messages);
            });

        });

        function applyFilters(path) {
            var stepValue = (parseInt(localStorage.getItem("historyStep")) + 1);
            localStorage.setItem("historyStep", stepValue);
            document.getElementById('loading-popup-id').style.display = "block";
            console.log("json data appl1", jsonData);
            BrowseUtil.cleanJsonData(jsonData);
            console.log("json data appl2", jsonData);

            var filter = JSON.stringify(jsonData);
            var requestUrl = '//' + location.host + location.pathname + "/json" +
                    "?filter=" + encodeURIComponent(filter);
            var displayUrl = requestUrl.replace('/json', '');
            window.history.pushState(requestUrl, '', displayUrl);

            $.ajax({
                url: requestUrl,
                method: 'get',
                success: function (response) {
                    renderData(response);
                    document.getElementById('loading-popup-id').style.display = "none";
                },
                error: function () {
                    document.getElementById('error-loading-popup-id').style.display = "block";
                }
            });
        }

        function renderData(pagedData) {
            renderLinks(pagedData);
            renderOverview(pagedData);
        }

        function renderLinks(pagedData) {
            var re = /filter=(.*)/;
            $("#exploreLink").attr("href", $("#exploreLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
            $("#mapLink").attr("href", $("#mapLink").attr("href").replace(re, "filter=" + encodeURIComponent(JSON.stringify(jsonData))));
        }

        function renderOverview(pagedData) {
            $("#mode-content").remove();

            var divModeContent = $("<div id='mode-content'/>");

            $("<h3/>")
                    .addClass("row row-section-heading")
                    .append($("<span class='glyphicon glyphicon-leaf'/>"))
                    .append(messages["data-overview.institutes"])
                    .appendTo(divModeContent);
            renderRows(
                    ["filter.institute.code", "filter.institute.country.iso3"],
                    ["statsInstCode", "statsInstCountry"],
                    ["instCode", "country"], divModeContent, pagedData);

            $("<h3/>")
                    .addClass("row row-section-heading")
                    .append($("<span class='glyphicon glyphicon-leaf'/>"))
                    .append(messages["data-overview.composition"])
                    .appendTo(divModeContent);
            renderRows(
                    ["filter.crops", "filter.sampStat"],
                    ["statsCrops", "statsSampStat"],
                    ["crop", "i18n.accession.sampleStatus"], divModeContent, pagedData);
            renderRows(
                    ["filter.taxonomy.genus", "filter.taxonomy.species"],
                    ["statsGenus", "statsSpecies"],
                    ["genus", "species"], divModeContent, pagedData);

            $("<h3/>")
                    .addClass("row row-section-heading")
                    .append($("<span class='glyphicon glyphicon-leaf'/>"))
                    .append(messages["data-overview.sources"])
                    .appendTo(divModeContent);
            renderRows(
                    ["filter.orgCty.iso3", "data-overview.donorCode"],
                    ["statsOrgCty", "statsDonorCode"],
                    ["country", "instCode"], divModeContent, pagedData);

            $("<h3/>")
                    .addClass("row row-section-heading")
                    .append($("<span class='glyphicon glyphicon-leaf'/>"))
                    .append(messages["data-overview.availability"])
                    .appendTo(divModeContent);
            renderRows(
                    ["data-overview.mlsStatus", "filter.available", "filter.historic"],
                    ["statsMLS", "statsAvailable", "statsHistoric"],
                    ["bool", "bool", "bool"], divModeContent, pagedData);

            $("<h3/>")
                    .addClass("row row-section-heading")
                    .append($("<span class='glyphicon glyphicon-leaf'/>"))
                    .append(messages["data-overview.management"])
                    .appendTo(divModeContent);
            renderRows(
                    ["filter.duplSite", "filter.sgsv"],
                    ["statsDuplSite", "statsSGSV"],
                    ["instCode", "bool"], divModeContent, pagedData);
            renderRows(
                    ["filter.storage"],
                    ["statsStorage"],
                    ["i18n.accession.storage"], divModeContent, pagedData);


            $("#content-area").append(divModeContent);
        }

        function renderRows(messageCodes, statsNames, types, parentDiv, pagedData) {
            var divRow = $("<div/>").addClass("row");
            for (var i = 0; i < messageCodes.length; i++) {
                var message = messages[messageCodes[i]];
                renderTermList(pagedData[statsNames[i]], message, types[i], divRow);
            }
            divRow.appendTo(parentDiv);
        }

        function renderTermList(stats, message, type, parentDiv) {
            var terms = stats.terms;
            var div = $("<div/>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-12 row-section");
            $("<h4/>").addClass("section-heading").text(message).appendTo(div);
            div.appendTo(parentDiv);

            var div2 = $("<div/>").addClass("section-inner-content clearfix")

            var termTable = $("<div/>").addClass("terms-table");
            for (var i = 0; i < terms.length; i++) {
                var divRow = $("<div/>").addClass("row");
                var divText = $("<div/>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-6").appendTo(divRow);
                var span = $("<span/>");
                var divCount = $("<div/>");
                var spanCount = $("<span/>");

                switch (type) {
                    case "country":
                    {
                        $("<a />", {
                            href: "/geo/" + terms[i].term.toUpperCase(),
                            text: messages["geo.iso3166-3." + terms[i].term]
                        }).appendTo(divText);
                        break;
                    }
                    case "crop":
                    {
                        span.text(messages[terms[i].term]).appendTo(divText);
                        break;
                    }
                    case "instCode":
                    {
                        $("<a />", {
                            href: "/wiews/" + terms[i].term.toUpperCase(),
                            text: terms[i].term.toUpperCase()
                        }).appendTo(divText);
                        break;
                    }
                    case "bool":
                    {
                        if (terms[i].term == "T") {
                            span.text(messages["boolean.true"]).appendTo(divText);
                        }
                        else if (terms[i].term == "F") {
                            span.text(messages["boolean.false"]).appendTo(divText);
                        }
                        break;
                    }
                    case "genus":
                    {
                        $("<a />", {
                            href: "/acn/t/" + terms[i].term,
                            text: terms[i].term
                        }).appendTo(divText);
                        break;
                    }
                    case "species":
                    {
                        span.text(terms[i].term).appendTo(divText);
                        break;
                    }
                    default:
                    {
                        if (type.startsWith('i18n')) {
                            var key = type.substring(5) + '.' + terms[i].term;
                            span.addClass("sci-name").text(messages[key]).appendTo(divText);
                        }
                        else {
                            span.text(terms[i].term).appendTo(divText);
                        }
                    }
                }

                divCount.addClass("col-lg-6 col-md-6 col-sm-6 col-xs-6").appendTo(divRow);
                var count = terms[i].count.toLocaleString();
                spanCount.addClass("stats-number").text(count).appendTo(divCount);
                divRow.appendTo(termTable);
            }
            renderAdditionalRow(stats.otherCount, "data-overview.otherCount", termTable);
            renderAdditionalRow(stats.missingCount, "data-overview.missingCount", termTable);
            termTable.appendTo(div2);
            div2.appendTo(div);
        }

        function renderAdditionalRow(count, messageCode, parentDiv) {
            if (count > 0) {
                parentDiv.append($("<div/>").addClass("row")
                        .append($("<div/>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-6")
                                .append($("<span/>")
                                        .append($("<em />").text(messages[messageCode]))))
                        .append($("<div/>").addClass("col-lg-6 col-md-6 col-sm-6 col-xs-6")
                                .append($("<span/>").addClass("stats-number").text(count.toLocaleString()))));
            }
        }

    </script>
</content>
</body>
</html>
