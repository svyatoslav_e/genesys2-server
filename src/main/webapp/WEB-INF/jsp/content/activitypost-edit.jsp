<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><c:out value="${acitivtypost.title}" escapeXml="false" /></title>
</head>
<body class="article-page selected-list">
	<h1 class="green-bg">
		<spring:message code="activitypost" />
	</h1>

	<form role="form" class="form-horizontal" action="<c:url value="/content/activitypost/update" />" method="post">
		<c:if test="${activityPost.id ne null}">
			<input type="hidden" name="id" value="${activityPost.id}" />
		</c:if>
		<div class="form-group">
			<label for="post-title" class="control-label"><spring:message code="activitypost.post-title" /></label>
			<div class="controls">
				<textarea id="post-title" name="title" class="span9 required html-editor">
					<c:out value="${activityPost.title}" escapeXml="false" />
				</textarea>
			</div>
		</div>
		<div class="form-group">
			<label for="post-body" class="control-label"><spring:message code="activitypost.post-body" /></label>
			<div class="controls">
				<textarea id="post-body" name="body" class="span9 required html-editor">
					<c:out value="${activityPost.body}" escapeXml="false" />
				</textarea>
			</div>
		</div>

		<div class="form-group transparent">
			<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
			<c:if test="${activityPost.id ne null}">
				<a class="btn btn-default" href="<c:url value="/content/activitypost/${activityPost.id}/delete" />"><spring:message code="delete" /></a>
			</c:if>
			<a class="btn btn-default" href="<c:url value="/" />"><spring:message code="cancel" /></a>
		</div>
        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>

<content tag="javascript">
	<script type="text/javascript">
		<local:tinyMCE selector="#post-title.html-editor" height="100" />
		<local:tinyMCE selector="#post-body.html-editor" />
	</script>
</content>
</body>
</html>

