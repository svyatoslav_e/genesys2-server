<%@ include file="/WEB-INF/jsp/init.jsp" %>
<html>
<head>
<title><spring:message code="charts" /></title>
</head>
<body>

	<cms:informative-h1 title="charts" fancy="true" info="charts.intro" />

	<div class="main-col-header clearfix">
		<div class="row">
			<div class="col-xs-9 pull-right text-right">
				<a class="btn btn-default" href="<c:url value="/explore"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-list"></span><span style="margin-left: 0.5em;"><spring:message code="view.accessions" /></span></a>
				<a class="btn btn-default" href="<c:url value="/explore/overview"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-eye-open"></span><span><spring:message code="data-overview.short" /></span></a>
				<a class="btn btn-default" href="<c:url value="/explore/map"><c:param name="filter" value="${jsonFilter}" /></c:url>"><span class="glyphicon glyphicon-globe"></span><span style="margin-left: 0.5em;"><spring:message code="maps.view-map" /></span></a>
			</div>
			<div class="col-xs-3 results">
				<a class="btn btn-default" href="javascript: window.history.go(-1);"><spring:message code="navigate.back" /></a>
			</div>
		</div>
	</div>

	<c:if test="${fn:length(currentFilters) gt 0}">
	<div id="allfilters" class="disabled applied-filters">
		<%-- Only render currently present filters --%>
        <c:forEach items="${currentFilters}" var="filter">

            <c:set var="normalizedKey" value="${filter.key.replace('.', '-').replace(':', '_')}"/>
            <c:set var="appliedFilter" value="${appliedFilters.get(filter.key)}" />

            <div class="clearfix filter-block" id="<c:out value="${normalizedKey}" />_filter" norm-key="<c:out value="${normalizedKey}" />" i-key="<c:out value="${filter.key}" />">
                <div class="col-lg-3 edit-fil">
                    <c:if test="${not filter.core}">
                        <c:out value="${filter.title}" />
                        <%-- <a href="<c:url value="/descriptors/${filter.key}" />"> --%>
                    </c:if>

                    <c:if test="${filter.core}">
						<spring:message code="filter.${filter.key}" />
                    </c:if>
                </div>
                <div class="col-lg-9">
                	<div class="filter-values" id="<c:out value="${normalizedKey}" />_value">
                		<c:if test="${appliedFilter.inverse}">
                			<spring:message code="filter.inverse" />
                		</c:if>
	                    <c:forEach items="${filters[appliedFilter.key]}" var="value">
	                        <c:set var="string" value="${value}"/>
	                        <c:if test="${fn:contains(value, 'range')}">
	                            <c:set var="string" value="${fn:replace(value,'{range=[','Between ')}"/>
	                            <c:set var="string" value="${fn:replace(string,',',' and ')}"/>
	                            <c:set var="string" value="${fn:replace(string,']}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'min')}">
	                            <c:set var="string" value="${fn:replace(value,'{min=','More than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
	                        <c:if test="${fn:contains(value, 'max')}">
	                            <c:set var="string" value="${fn:replace(value,'{max=','Less than ')}"/>
	                            <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":')}"/>
	                        </c:if>
                            <c:if test="${fn:contains(value, 'like')}">
                                <c:set var="string" value="${fn:replace(value,'{like=','Like ')}"/>
                                <c:set var="string" value="${fn:replace(string,'}','')}"/>
                                <c:set var="value" value="${fn:replace(value,'{','{\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'=','\":\"')}"/>
                                <c:set var="value" value="${fn:replace(value,'}','\"}')}"/>
                            </c:if>

	                        <c:if test="${string==null}">
	                        	<c:set var="string" value="null" />
	                        	<c:set var="value" value="null" />
	                        </c:if>
	                        <div class="filtval complex" x-key="<c:out value="${normalizedKey}" /><c:out value="${value}"/>" i-key="<c:out value="${filter.key}" />"><c:out value="${string}" /></div>
	                        <c:remove var="string" />
	                    </c:forEach>
	                </div>
                </div>
            </div>
        </c:forEach>
    </div>
    </c:if>
	

<div id="container" style="min-height: 500px; min-width: 310px; margin: 0 auto"></div>

<content tag="javascript">
    <script type="text/javascript" src="<c:url value="/html/js/genesyshighcharts.min.js" />"></script>
    <script type="text/javascript">
        $(function () {
            'use strict';

            $.getJSON('/${pageContext.response.locale}/explore/0/charts/data/country-collection-size' + window.location.search, function (data) {
                var mapData = Highcharts.geojson(Highcharts.maps['custom/world']);

                $('#container').highcharts('Map', {
                    chart: {
                        borderWidth: 0
                    },

                    title: {
                        text: '<spring:message code="chart.collections.title" />'
                    },
<%--
                    subtitle: {
                        text: '<spring:message code="chart.attribution-text" />' 
                    },
--%>
                    legend: {
                        enabled: false
                    },

                    mapNavigation: {
                        enabled: true,
                        buttonOptions: {
                            verticalAlign: 'top'
                        }
                    },

                    series: [{
                        name: 'World',
                        mapData: mapData,
                        color: '#88ba42',
                        enableMouseTracking: false
                    }, {
                        type: 'mapbubble',
                        mapData: mapData,
                        name: '<spring:message code="chart.collections.series" />',
                        joinBy: ['iso-a2', 'code'],
                        data: data,
                        sizeBy: 'width',
                        color: '#88ba42',
                        minSize: '3',
                        maxSize: '30%',
                        tooltip: {
                            pointFormat: '{point.country}: {point.z}'
                        }
                    }]
                });
            });
        });
    </script>
</content>
</body>
</html>
