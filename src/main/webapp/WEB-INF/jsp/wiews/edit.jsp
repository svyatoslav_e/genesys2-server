<!DOCTYPE html>

<%@ include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
<title><spring:message code="faoInstitutes.page.profile.title" arguments="${faoInstitute.fullName}" argumentSeparator="|" /></title>
</head>
<body>
	<h1 class="green-bg">
		<%--<img class="country-flag bigger" src="<c:url value="${cdnFlagsUrl}/${faoInstitute.country.code3.toUpperCase()}.svg" />" />--%>
		<c:out value="${faoInstitute.fullName}" />
		<small><c:out value="${faoInstitute.code}" /></small>
	</h1>

	<form action="<c:url value="/wiews/${faoInstitute.code}/update" />" method="post" class="form-horizontal">
		<div class="form-group">
			<label for="blurp-body" class="col-lg-12 control-label"><spring:message code="blurp.blurp-body" /></label>
			<div class="controls col-lg-12">
				<textarea id="blurp-body" name="blurp" class="span9 required html-editor">
					<c:out value="${blurp.body}" />
				</textarea>
			</div>
		</div>
		
		<div class="form-group">
			<label for="faoInstitute-summary" class="col-lg-12 control-label"><spring:message code="faoInstitute.summary" /></label>
			<div class="controls col-lg-12">
				<textarea id="faoInstitute-summary" name="summary" class="span9 required html-editor">
					<c:out value="${blurp.summary}" />
				</textarea>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-5 col-lg-3 col-md-12 col-sm-12 col-xs-12 control-label"><spring:message code="ga.tracker-code" /></label>
			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 controls">
				<input type="text" name="gaTracker" class="form-control required" value="${faoInstitute.settings['googleAnalytics.tracker'].value}" />
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-5 col-md-12 col-sm-12 col-xs-12 control-label"><spring:message code="faoInstitute.requests.mailto" /></label>
			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 controls">
				<input type="text" name="mailto" class="form-control required" value="${faoInstitute.settings['requests.mailto'].value}" />
			</div>
		</div>

        <div class="form-group">
            <label class="col-lg-5 col-md-12 col-sm-12 col-xs-12 control-label"><spring:message code="faoInstitute.sgsv" /></label>
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 controls">
                <input type="text" name="codeSVGS" class="form-control required" value="${faoInstitute.codeSGSV}" />
            </div>
        </div>

		<div class="form-group">
			<div class="col-lg-offset-5 col-lg-7 col-md-12 col-sm-12 col-xs-12 controls">
				<label><input type="radio" name="uniqueAcceNumbs" class="" value="true" ${faoInstitute.uniqueAcceNumbs==true ? 'checked' : ''} /> <spring:message code="faoInstitute.uniqueAcceNumbs.true" /></label>
				<label><input type="radio" name="uniqueAcceNumbs" class="" value="false" ${faoInstitute.uniqueAcceNumbs==false ? 'checked' : ''} /> <spring:message code="faoInstitute.uniqueAcceNumbs.false" /></label>
			</div>
            <div class="col-lg-offset-5 col-lg-7 col-md-12 col-sm-12 col-xs-12 controls">
            <label><input type="checkbox" name="allowMaterialRequests" class=""  ${faoInstitute.allowMaterialRequests==true ? 'checked' : ''} /> <spring:message code="faoInstitute.allow.requests" /></label>
            </div>
        </div>

		<div class="form-group transparent">
			<div class="col-lg-offset-5 col-lg-7 col-md-12 col-sm-12 col-xs-12">
				<input type="submit" value="<spring:message code="save"/>" class="btn btn-primary" />
				<a href="<c:url value="/wiews/${faoInstitute.code}" />" class="btn btn-default"> <spring:message code="cancel" /></a>
			</div>
		</div>

        <!-- CSRF protection -->
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	</form>


<content tag="javascript">
  <script type="text/javascript">
	<local:tinyMCE selector=".html-editor" />
  </script>
</content>

</body>
</html>